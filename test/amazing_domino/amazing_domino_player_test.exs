defmodule AmazingDominoPlayerTest do
  use ExUnit.Case
  doctest AmazingDomino.Engine.PlayerOperations

  alias AmazingDomino.Engine.PlayerOperations
  alias AmazingDomino.Engine.PlayerOperations.Player

  test "show dominoes for player" do
    player = %Player{
      nick_name: "player1",
      dominoes: [{0, 0}, {0, 1}, {0, 2}, {0, 3}, {0, 4}, {0, 5}, {0, 6}]
    }

    assert PlayerOperations.show_dominoes(player) == [
             {0, 0},
             {0, 1},
             {0, 2},
             {0, 3},
             {0, 4},
             {0, 5},
             {0, 6}
           ]
  end

  test "get validate dominoes from player when board is empty and when player has mules dominoes" do
    player = %Player{
      dominoes: [{1, 6}, {3, 3}, {6, 6}, {3, 4}, {2, 5}, {1, 2}, {0, 1}],
      nick_name: "player2"
    }

    assert [{6, 6}] == PlayerOperations.get_validate_dominoes_to_play(player, [])
  end

  test "get validate dominoes from player when board is empty and when player hasn't mules dominoes" do
    player = %Player{
      dominoes: [{1, 6}, {2, 3}, {5, 6}, {3, 4}, {2, 5}, {1, 2}, {0, 1}],
      nick_name: "player2"
    }

    assert [{5, 6}] == PlayerOperations.get_validate_dominoes_to_play(player, [])
  end

  test "get validate dominoes from player when board isn't empty" do
    player = %Player{
      dominoes: [{3, 5}, {2, 3}, {0, 5}, {3, 6}, {4, 6}, {4, 4}, {4, 5}],
      nick_name: "player1"
    }

    board = [{0, 6}, {6, 6}, {6, 2}]

    assert [{2, 3}, {0, 5}] == PlayerOperations.get_validate_dominoes_to_play(player, board)
  end

  test "play a domino by first time" do
    player = %Player{
      dominoes: [{3, 5}, {2, 3}, {0, 5}, {3, 6}, {4, 6}, {4, 4}, {4, 5}],
      nick_name: "player1"
    }

    domino_to_play = {3, 6}
    matched_domino = {3, 6}
    updated_player = PlayerOperations.play_a_domino(player, domino_to_play, matched_domino)

    assert updated_player.dominoes == [{3, 5}, {2, 3}, {0, 5}, {4, 6}, {4, 4}, {4, 5}]
    assert updated_player.played_dominoes == [{3, 6}]
  end

  test "play a domino no first time" do
    player = %Player{
      dominoes: [{3, 5}, {2, 3}, {0, 5}, {4, 6}, {4, 4}, {4, 5}],
      nick_name: "player1",
      played_dominoes: [{3, 6}]
    }

    domino_to_play = {2, 3}
    matched_domino = {3, 2}
    updated_player = PlayerOperations.play_a_domino(player, domino_to_play, matched_domino)

    assert updated_player.dominoes == [{3, 5}, {0, 5}, {4, 6}, {4, 4}, {4, 5}]
    assert updated_player.played_dominoes == [{3, 2}, {3, 6}]
  end

  test "add a domino into player dominoes" do
    player = %Player{
      dominoes: [{3, 5}, {2, 3}, {0, 5}, {4, 6}, {4, 4}, {4, 5}],
      nick_name: "player1",
      played_dominoes: [{3, 6}]
    }

    domino = [{2, 2}]
    updated_player = PlayerOperations.add_a_domino(player, domino)

    assert updated_player.dominoes == player.dominoes ++ domino
  end

  test "get points for current player dominoes" do
    player = %Player{
      dominoes: [{3, 5}, {2, 3}, {0, 5}, {4, 6}, {4, 4}, {4, 5}],
      nick_name: "player1",
      played_dominoes: [{3, 6}]
    }

    current_points = PlayerOperations.get_current_points(player)

    assert current_points == 45
  end
end
