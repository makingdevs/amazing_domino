defmodule AmazingDominoGameTest do
  use ExUnit.Case
  doctest AmazingDomino.Engine.GameOperations
  alias AmazingDomino.Engine.GameOperations
  alias AmazingDomino.Engine.GameOperations.Game
  alias AmazingDomino.Engine.PlayerOperations.Player

  defp get_game_with_two_players_confirmed do
    pl1 = %Player{nick_name: "player1"}
    pl2 = %Player{nick_name: "player2"}
    {:ok, the_game} = GameOperations.create()
    {:ok, the_game} = GameOperations.add_player(the_game, {pl1, :confirmed})
    GameOperations.add_player(the_game, {pl2, :confirmed})
  end

  test "create a game" do
    {:ok, created_game} = GameOperations.create()
    assert created_game.uuid != ""
    assert created_game.players == []
    assert created_game.status == :created

    assert created_game.dominoes == [
             {0, 0},
             {0, 1},
             {0, 2},
             {0, 3},
             {0, 4},
             {0, 5},
             {0, 6},
             {1, 1},
             {1, 2},
             {1, 3},
             {1, 4},
             {1, 5},
             {1, 6},
             {2, 2},
             {2, 3},
             {2, 4},
             {2, 5},
             {2, 6},
             {3, 3},
             {3, 4},
             {3, 5},
             {3, 6},
             {4, 4},
             {4, 5},
             {4, 6},
             {5, 5},
             {5, 6},
             {6, 6}
           ]
  end

  test "generate dominoes" do
    assert GameOperations.generate_dominoes() == [
             {0, 0},
             {0, 1},
             {0, 2},
             {0, 3},
             {0, 4},
             {0, 5},
             {0, 6},
             {1, 1},
             {1, 2},
             {1, 3},
             {1, 4},
             {1, 5},
             {1, 6},
             {2, 2},
             {2, 3},
             {2, 4},
             {2, 5},
             {2, 6},
             {3, 3},
             {3, 4},
             {3, 5},
             {3, 6},
             {4, 4},
             {4, 5},
             {4, 6},
             {5, 5},
             {5, 6},
             {6, 6}
           ]
  end

  test "confirm join for a player" do
    players = [
      {%Player{nick_name: "player1"}, :confirmed},
      {%Player{nick_name: "player2"}, :confirmed},
      {%Player{nick_name: "player4"}, :pending}
    ]

    {:ok, the_game} = GameOperations.create()
    the_game = %Game{the_game | players: players}
    player_joining = "player4"

    {_, game} = GameOperations.confirm_join(the_game, player_joining)

    assert game.players |> Enum.at(2) == {%Player{nick_name: "player4"}, :confirmed}
  end

  test "cancel join for a player" do
    players = [
      {%Player{nick_name: "player1"}, :confirmed},
      {%Player{nick_name: "player2"}, :confirmed},
      {%Player{nick_name: "player4"}, :pending}
    ]

    {:ok, the_game} = GameOperations.create()
    the_game = %Game{the_game | players: players}
    player_joining = "player4"

    {_, game} = GameOperations.cancel_join(the_game, player_joining)

    assert Enum.count(game.players) == 2

    assert not Enum.member?(
             game.players |> Enum.map(fn {player, _status} -> player.nick_name end),
             "player4"
           )
  end

  test "not cancel join when game is not created" do
    players = [
      {%Player{nick_name: "player1"}, :confirmed},
      {%Player{nick_name: "player2"}, :confirmed},
      {%Player{nick_name: "player4"}, :pending}
    ]

    {:ok, the_game} = GameOperations.create()
    the_game = %Game{the_game | status: :started, players: players}
    player_joining = "player4"

    {:error, message} = GameOperations.cancel_join(the_game, player_joining)

    assert message == "El juego ya fue iniciado"
  end

  test "starting the game" do
    {:ok, the_game} = get_game_with_two_players_confirmed()
    {:ok, started_game} = GameOperations.start(the_game)

    assert started_game.status == :started
  end

  test "don't start the game because is closed" do
    {:ok, the_game} = get_game_with_two_players_confirmed()
    {:error, message} = GameOperations.start(%Game{the_game | status: :closed})

    assert message == "Can't start the game because current status is not valid"
  end

  test "don't start the game because there aren't enough players yet" do
    players = [
      {%Player{nick_name: "player1"}, :confirmed}
    ]

    {:ok, the_game} = GameOperations.create()
    the_game = %Game{the_game | players: players}
    {:error, message} = GameOperations.start(the_game)

    assert message == "Not enough players yet"
  end

  test "share the game" do
    {:ok, the_game} = get_game_with_two_players_confirmed()
    assert GameOperations.share(the_game) != ""
    assert GameOperations.share(the_game) == the_game.uuid
  end

  test "add a player into game" do
    the_player = %Player{nick_name: "another player"}

    {:ok, the_game} = get_game_with_two_players_confirmed()
    {:ok, updated_game} = GameOperations.add_player(the_game, {the_player, :pending})

    assert Enum.count(updated_game.players) == 3
    assert the_game.uuid == updated_game.uuid
    assert updated_game.players == the_game.players ++ [{the_player, :pending}]
  end

  test "don't add a player because the game status is not valid" do
    the_player = %Player{nick_name: "another player"}

    {:ok, the_game} = get_game_with_two_players_confirmed()
    {:ok, started_game} = GameOperations.start(the_game)

    {:error, message} = GameOperations.add_player(started_game, {the_player, :pending})

    assert message == "Can't add the player because status is not valid"
  end

  test "don't add a player because the game already has max (4) players" do
    another_player = %Player{nick_name: "another player"}

    players = [
      {%Player{nick_name: "player1"}, :confirmed},
      {%Player{nick_name: "player2"}, :confirmed},
      {%Player{nick_name: "player3"}, :confirmed},
      {%Player{nick_name: "player4"}, :confirmed}
    ]

    {:ok, the_game} = GameOperations.create()
    the_game = %Game{the_game | players: players}

    {:error, message} = GameOperations.add_player(the_game, {another_player, :pending})

    assert message ==
             "Esta partida tiene 4 jugadores. Únete a otra partida o inicia un juego nuevo"
  end

  test "don't add a player because the game is closed" do
    another_player = %Player{nick_name: "another player"}

    players = [
      {%Player{nick_name: "player1"}, :confirmed},
      {%Player{nick_name: "player2"}, :confirmed},
      {%Player{nick_name: "player3"}, :confirmed},
      {%Player{nick_name: "player4"}, :confirmed}
    ]

    {:ok, the_game} = GameOperations.create()
    the_game = %Game{the_game | players: players, status: :close}

    {:error, message} = GameOperations.add_player(the_game, {another_player, :pending})

    assert message == "Can't add the player because status is not valid"
  end

  test "don't add a player because the game has already delivered dominoes" do
    another_player = %Player{nick_name: "another player"}

    {:ok, the_game} = get_game_with_two_players_confirmed()
    {:ok, the_game_with_delivered_dominoes} = GameOperations.hand_out_dominoes(the_game)

    {:error, message} =
      GameOperations.add_player(the_game_with_delivered_dominoes, {another_player, :pending})

    assert message == "Can't add the player because status is not valid"
  end

  test "don't add a player because it already exists in the game" do
    repeated_player = %Player{nick_name: "player1"}
    {:ok, the_game} = get_game_with_two_players_confirmed()

    {:error, message} = GameOperations.add_player(the_game, {repeated_player, :pending})

    assert message == "El jugador --#{repeated_player.nick_name}-- ya existe"
  end

  test "hand out dominoes to players in the game with two players" do
    {:ok, the_game} = get_game_with_two_players_confirmed()
    {:ok, the_game_with_delivered_dominoes} = GameOperations.hand_out_dominoes(the_game)

    result_players =
      the_game_with_delivered_dominoes.players
      |> Enum.map(fn {player, _status} -> player end)

    assert Enum.count(Enum.at(result_players, 0).dominoes) == 7
    assert Enum.count(Enum.at(result_players, 1).dominoes) == 7
    assert Enum.count(the_game_with_delivered_dominoes.dominoes) == 14
    assert Enum.at(result_players, 0).dominoes != Enum.at(result_players, 1).dominoes
    assert the_game_with_delivered_dominoes.status == :delivered
  end

  test "hand out dominoes to players in the game with four players" do
    players = [
      {%Player{nick_name: "player1"}, :confirmed},
      {%Player{nick_name: "player2"}, :confirmed},
      {%Player{nick_name: "player3"}, :confirmed},
      {%Player{nick_name: "player4"}, :confirmed}
    ]

    {:ok, the_game} = GameOperations.create()
    the_game = %Game{the_game | players: players}
    {:ok, the_game_with_delivered_dominoes} = GameOperations.hand_out_dominoes(the_game)

    result_players =
      the_game_with_delivered_dominoes.players
      |> Enum.map(fn {player, _status} -> player end)

    assert Enum.count(Enum.at(result_players, 0).dominoes) == 7
    assert Enum.count(Enum.at(result_players, 1).dominoes) == 7
    assert Enum.count(Enum.at(result_players, 2).dominoes) == 7
    assert Enum.count(Enum.at(result_players, 3).dominoes) == 7
    assert Enum.count(the_game_with_delivered_dominoes.dominoes) == 0
    assert the_game_with_delivered_dominoes.status == :delivered
  end

  test "don't hand out dominoes to players in started game" do
    {:ok, the_game} = get_game_with_two_players_confirmed()
    {:ok, started_game} = GameOperations.start(the_game)
    {:error, message} = GameOperations.hand_out_dominoes(started_game)

    assert message == "Can't hand out dominoes because current status isn't valid"
  end

  test "don't hand out dominoes to players in closed game" do
    {:ok, the_game} = get_game_with_two_players_confirmed()
    the_game = %Game{the_game | status: :close}
    {:error, message} = GameOperations.hand_out_dominoes(the_game)

    assert message == "Can't hand out dominoes because current status isn't valid"
  end

  test "don't hand out dominoes to players in delivered game" do
    {:ok, the_game} = get_game_with_two_players_confirmed()
    {:ok, the_game_with_delivered_dominoes} = GameOperations.hand_out_dominoes(the_game)
    {:error, message} = GameOperations.hand_out_dominoes(the_game_with_delivered_dominoes)

    assert message == "Can't hand out dominoes because current status isn't valid"
  end

  test "calculate first turn non players has mules dominoes" do
    all_players = [
      {%Player{
         dominoes: [{4, 5}, {2, 3}, {2, 4}, {3, 5}, {0, 3}, {1, 6}, {1, 4}],
         nick_name: "player1"
       }, :confirmed},
      {%Player{
         dominoes: [{3, 4}, {0, 5}, {2, 5}, {5, 6}, {1, 3}, {0, 2}, {0, 4}],
         nick_name: "player2"
       }, :confirmed}
    ]

    {player, first_turn} = GameOperations.calculate_first_turn([], all_players, :mules)

    assert first_turn == 1
    assert player.nick_name == "player2"
  end

  test "calculate first turn when only one player hasn't mules dominoes" do
    players_with_mules = [
      {%Player{
         dominoes: [{3, 3}, {4, 4}, {2, 4}, {3, 6}, {0, 3}, {1, 6}, {1, 4}],
         nick_name: "player1"
       }, :confirmed},
      {%Player{
         dominoes: [{5, 5}, {0, 5}, {2, 5}, {5, 6}, {1, 3}, {0, 2}, {0, 4}],
         nick_name: "player2"
       }, :confirmed}
    ]

    all_players = [
      {%Player{
         dominoes: [{3, 3}, {2, 2}, {2, 4}, {3, 6}, {0, 3}, {1, 6}, {1, 4}],
         nick_name: "player1"
       }, :confirmed},
      {%Player{
         dominoes: [{0, 1}, {1, 2}, {2, 3}, {3, 4}, {4, 5}, {0, 6}, {1, 5}],
         nick_name: "player3"
       }, :confirmed},
      {%Player{
         dominoes: [{4, 4}, {0, 5}, {2, 5}, {5, 6}, {1, 3}, {0, 2}, {0, 4}],
         nick_name: "player2"
       }, :confirmed}
    ]

    {player, first_turn} =
      GameOperations.calculate_first_turn(players_with_mules, all_players, :mules)

    assert first_turn == 2
    assert player.nick_name == "player2"
  end

  test "change to next turn in the game when current turn isn't the last player" do
    players = [
      {%Player{nick_name: "player1"}, :confirmed},
      {%Player{nick_name: "player2"}, :confirmed},
      {%Player{nick_name: "player3"}, :confirmed}
    ]

    {:ok, the_game} = GameOperations.create()
    the_game = %Game{the_game | players: players}
    {:ok, started_game} = GameOperations.start(the_game)

    game_to_test = %Game{started_game | current_turn: 1}

    {:ok, change_turn_game} = GameOperations.change_to_next_turn(game_to_test)

    assert change_turn_game.current_turn == game_to_test.current_turn + 1
  end

  test "change to next turn in the game when current turn is the last player" do
    players = [
      {%Player{nick_name: "player1"}, :confirmed},
      {%Player{nick_name: "player2"}, :confirmed},
      {%Player{nick_name: "player3"}, :confirmed}
    ]

    {:ok, the_game} = GameOperations.create()
    the_game = %Game{the_game | players: players}
    {:ok, started_game} = GameOperations.start(the_game)

    {:ok, change_turn_game} =
      GameOperations.change_to_next_turn(%Game{started_game | current_turn: 2})

    assert change_turn_game.current_turn == 0
  end

  test "get player in turn" do
    players = [
      {%Player{nick_name: "player1"}, :confirmed},
      {%Player{nick_name: "player2"}, :confirmed},
      {%Player{nick_name: "player3"}, :confirmed}
    ]

    {:ok, the_game} = GameOperations.create()
    the_game = %Game{the_game | players: players}
    game_to_test = %Game{the_game | current_turn: 1}

    assert players |> Enum.map(fn {player, _status} -> player end) |> Enum.at(1) ==
             GameOperations.get_player_in_turn(game_to_test)
  end

  test "matching domino to play with right side board domino when already match" do
    right_board_domino = 4
    domino_to_play = {4, 5}
    matched_domino = GameOperations.match_domino_to_right(right_board_domino, domino_to_play)

    assert matched_domino == domino_to_play
  end

  test "matching domino to play with right side board domino when it is inverse" do
    right_board_domino = 5
    domino_to_play = {4, 5}
    matched_domino = GameOperations.match_domino_to_right(right_board_domino, domino_to_play)

    assert matched_domino == {5, 4}
  end

  test "get error when matching domino to play with right side board domino when don't match" do
    right_board_domino = 3
    domino_to_play = {4, 5}
    matched_domino = GameOperations.match_domino_to_right(right_board_domino, domino_to_play)

    assert matched_domino == {:error, "Ficha no válida en esta posición"}
  end

  test "matching domino to play with left side board domino when already match" do
    left_board_domino = 4
    domino_to_play = {3, 4}
    matched_domino = GameOperations.match_domino_to_left(left_board_domino, domino_to_play)

    assert matched_domino == domino_to_play
  end

  test "matching domino to play with left side board domino when it is inverse" do
    left_board_domino = 3
    domino_to_play = {3, 4}
    matched_domino = GameOperations.match_domino_to_left(left_board_domino, domino_to_play)

    assert matched_domino == {4, 3}
  end

  test "get error when matching domino to play with left side board domino when don't match" do
    left_board_domino = 3
    domino_to_play = {4, 5}
    matched_domino = GameOperations.match_domino_to_left(left_board_domino, domino_to_play)

    assert matched_domino == {:error, "Ficha no válida en esta posición"}
  end

  test "put domino in board when it's empty" do
    board = []
    domino = {4, 4}

    [{:board, new_board}, _, _] = GameOperations.put_domino_in_board(board, domino, :left)

    assert new_board == [{4, 4}]
  end

  test "put domino in board to the left when board isn't empty and domino to play is matching" do
    board = [{4, 4}, {4, 6}, {6, 6}]
    domino = {3, 4}

    [{:board, new_board}, {:matched_domino, matched_domino}, _] =
      GameOperations.put_domino_in_board(board, domino, :left)

    assert new_board == [{3, 4}, {4, 4}, {4, 6}, {6, 6}]
    assert matched_domino == {3, 4}
  end

  test "put domino in board to the left when board isn't empty and domino to play isn't matching" do
    board = [{4, 4}, {4, 6}, {6, 6}]
    domino = {4, 6}

    [{:board, new_board}, {:matched_domino, matched_domino}, _] =
      GameOperations.put_domino_in_board(board, domino, :left)

    assert new_board == [{6, 4}, {4, 4}, {4, 6}, {6, 6}]
    assert matched_domino == {6, 4}
  end

  test "get an error when put domino in board to the left side but it can't match with this side" do
    board = [{5, 2}, {3, 5}, {4, 3}]
    domino = {1, 4}

    [{:board, new_board}, {:error, message}] =
      GameOperations.put_domino_in_board(board, domino, :left)

    assert new_board == board
    assert message == "Ficha no válida en esta posición"
  end

  test "put domino in board to the right when board isn't empty and domino to play is matching" do
    board = [{4, 3}, {3, 5}, {5, 2}]
    domino = {2, 6}

    [{:board, new_board}, {:matched_domino, matched_domino}, _] =
      GameOperations.put_domino_in_board(board, domino, :right)

    assert new_board == [{4, 3}, {3, 5}, {5, 2}, {2, 6}]
    assert matched_domino == {2, 6}
  end

  test "put domino in board to the right when board isn't empty and domino to play isn't matching" do
    board = [{4, 3}, {3, 5}, {5, 2}]
    domino = {1, 2}

    [{:board, new_board}, {:matched_domino, matched_domino}, _] =
      GameOperations.put_domino_in_board(board, domino, :right)

    assert new_board == [{4, 3}, {3, 5}, {5, 2}, {2, 1}]
    assert matched_domino == {2, 1}
  end

  test "get an error when put domino in board to the right side but it can't match with this side" do
    board = [{4, 3}, {3, 5}, {5, 2}]
    domino = {1, 4}

    [{:board, new_board}, {:error, message}] =
      GameOperations.put_domino_in_board(board, domino, :right)

    assert new_board == board
    assert message == "Ficha no válida en esta posición"
  end

  test "make a play for player in turn successfully" do
    players = [
      {%Player{
         dominoes: [{4, 4}, {0, 4}, {0, 6}, {0, 2}, {2, 5}, {0, 3}],
         nick_name: "player3",
         played_dominoes: [{0, 5}]
       }, :confirmed},
      {%Player{
         dominoes: [{2, 3}, {1, 2}, {1, 3}, {1, 4}, {3, 6}, {3, 4}],
         nick_name: "player2",
         played_dominoes: [{4, 5}]
       }, :confirmed},
      {%Player{
         dominoes: [{2, 6}, {5, 6}, {3, 3}, {3, 5}, {0, 1}, {1, 5}],
         nick_name: "player1",
         played_dominoes: [{5, 5}]
       }, :confirmed}
    ]

    board = [{4, 5}, {5, 5}, {5, 0}]
    current_turn = 2
    selected_domino = {0, 1}
    selected_side = :right
    the_game = %Game{board: board, current_turn: current_turn, players: players, status: :started}

    {:ok, new_game} = GameOperations.make_a_play(the_game, selected_domino, selected_side)

    new_game_players =
      new_game.players
      |> Enum.map(fn {player, _status} -> player end)

    assert new_game.board == board ++ [selected_domino]

    assert Enum.at(new_game_players, current_turn).dominoes == [
             {2, 6},
             {5, 6},
             {3, 3},
             {3, 5},
             {1, 5}
           ]

    assert Enum.at(new_game_players, current_turn).played_dominoes == [{0, 1}, {5, 5}]
  end

  test "make a play for player in turn failed" do
    players = [
      {%Player{
         dominoes: [{4, 4}, {0, 4}, {0, 6}, {0, 2}, {2, 5}, {0, 3}],
         nick_name: "player3",
         played_dominoes: [{0, 5}]
       }, :confirmed},
      {%Player{
         dominoes: [{2, 3}, {1, 2}, {1, 3}, {1, 4}, {3, 6}, {3, 4}],
         nick_name: "player2",
         played_dominoes: [{4, 5}]
       }, :confirmed},
      {%Player{
         dominoes: [{2, 6}, {5, 6}, {3, 3}, {3, 5}, {0, 1}, {1, 5}],
         nick_name: "player1",
         played_dominoes: [{5, 5}]
       }, :confirmed}
    ]

    board = [{4, 5}, {5, 5}, {5, 0}]
    current_turn = 2
    selected_domino = {0, 1}
    selected_side = :left
    the_game = %Game{board: board, current_turn: current_turn, players: players, status: :started}

    {:error, message} = GameOperations.make_a_play(the_game, selected_domino, selected_side)

    assert message == "Ficha no válida en esta posición"
  end

  test "take a domino from well" do
    players = [
      {%Player{
         dominoes: [{4, 4}, {0, 4}, {0, 6}, {0, 2}, {2, 5}, {0, 3}],
         nick_name: "player3",
         played_dominoes: [{0, 5}]
       }, :confirmed},
      {%Player{
         dominoes: [{2, 3}, {1, 2}, {1, 3}, {1, 4}, {3, 6}, {3, 4}],
         nick_name: "player2",
         played_dominoes: [{4, 5}]
       }, :confirmed},
      {%Player{
         dominoes: [{2, 6}, {5, 6}, {3, 3}, {3, 5}, {1, 6}, {1, 5}],
         nick_name: "player1",
         played_dominoes: [{5, 5}]
       }, :confirmed}
    ]

    board = [{4, 5}, {5, 5}, {5, 0}]
    dominoes = [{0, 0}, {1, 1}, {0, 1}, {2, 2}, {2, 4}, {4, 6}, {6, 6}]
    current_turn = 2

    the_game = %Game{
      board: board,
      dominoes: dominoes,
      current_turn: current_turn,
      players: players,
      status: :started
    }

    {:ok, new_game} = GameOperations.take_domino_from_well(the_game)

    new_game_players =
      new_game.players
      |> Enum.map(fn {player, _status} -> player end)

    assert Enum.count(new_game.dominoes) == 6
    assert Enum.count(Enum.at(new_game_players, current_turn).dominoes) == 7
  end

  test "don't take a domino from well because it is empty" do
    players = [
      {%Player{
         dominoes: [{4, 4}, {0, 4}, {0, 6}, {0, 2}, {2, 5}, {0, 3}],
         nick_name: "player3",
         played_dominoes: [{0, 5}]
       }, :confirmed},
      {%Player{
         dominoes: [{2, 3}, {1, 2}, {1, 3}, {1, 4}, {3, 6}, {3, 4}],
         nick_name: "player2",
         played_dominoes: [{4, 5}]
       }, :confirmed},
      {%Player{
         dominoes: [{2, 6}, {5, 6}, {3, 3}, {3, 5}, {1, 6}, {1, 5}],
         nick_name: "player1",
         played_dominoes: [{5, 5}]
       }, :confirmed}
    ]

    board = [{4, 5}, {5, 5}, {5, 0}]
    dominoes = []
    current_turn = 2

    the_game = %Game{
      board: board,
      dominoes: dominoes,
      current_turn: current_turn,
      players: players,
      status: :started
    }

    {:error, message} = GameOperations.take_domino_from_well(the_game)

    assert message == "The well is empty"
  end

  test "game over by dominoes exhausted is true" do
    players = [
      {%Player{
         dominoes: [{4, 4}, {0, 4}, {0, 6}, {0, 2}, {2, 5}, {0, 3}],
         nick_name: "player3",
         played_dominoes: [{0, 5}]
       }, :confirmed},
      {%Player{
         dominoes: [],
         nick_name: "player2",
         played_dominoes: [{4, 5}]
       }, :confirmed},
      {%Player{
         dominoes: [{2, 6}, {5, 6}, {3, 3}, {3, 5}, {1, 6}, {1, 5}],
         nick_name: "player1",
         played_dominoes: [{5, 5}]
       }, :confirmed}
    ]

    the_game = %Game{players: players}

    game_over = GameOperations.is_game_over_by_dominoes_exhausted?(the_game)

    assert game_over
  end

  test "game over by dominoes exhausted is false" do
    players = [
      {%Player{
         dominoes: [{4, 4}, {0, 4}, {0, 6}, {0, 2}, {2, 5}, {0, 3}],
         nick_name: "player3",
         played_dominoes: [{0, 5}]
       }, :confirmed},
      {%Player{
         dominoes: [{2, 6}, {5, 6}, {3, 3}, {3, 5}, {1, 6}, {1, 5}],
         nick_name: "player2",
         played_dominoes: [{4, 5}]
       }, :confirmed},
      {%Player{
         dominoes: [{2, 6}, {5, 6}, {3, 3}, {3, 5}, {1, 6}, {1, 5}],
         nick_name: "player1",
         played_dominoes: [{5, 5}]
       }, :confirmed}
    ]

    the_game = %Game{players: players}

    game_over = GameOperations.is_game_over_by_dominoes_exhausted?(the_game)

    assert not game_over
  end

  test "finish game because a player has finished their dominoes" do
    players = [
      {%Player{
         dominoes: [{4, 4}, {0, 4}, {0, 6}, {0, 2}, {2, 5}, {0, 3}],
         nick_name: "player3",
         played_dominoes: [{0, 5}]
       }, :confirmed},
      {%Player{
         dominoes: [],
         nick_name: "player2",
         played_dominoes: [{4, 5}]
       }, :confirmed},
      {%Player{
         dominoes: [{2, 6}, {5, 6}, {3, 3}, {3, 5}, {1, 6}, {1, 5}],
         nick_name: "player1",
         played_dominoes: [{5, 5}]
       }, :confirmed}
    ]

    the_game = %Game{players: players}

    {:ok, game_over} = GameOperations.finish_game(the_game)

    assert game_over.status == :finished
  end

  test "game over by closed game is true" do
    players = [
      {%Player{
         dominoes: [{4, 4}],
         nick_name: "player3",
         played_dominoes: [{0, 3}, {2, 5}, {0, 4}, {0, 6}, {0, 2}, {0, 5}]
       }, :confirmed},
      {%Player{
         dominoes: [{1, 2}, {1, 3}, {1, 4}, {3, 6}],
         nick_name: "player2",
         played_dominoes: [{2, 3}, {3, 4}, {4, 5}]
       }, :confirmed},
      {%Player{
         dominoes: [{2, 6}, {5, 6}, {1, 6}, {1, 5}],
         nick_name: "player1",
         played_dominoes: [{3, 3}, {3, 5}, {5, 5}]
       }, :confirmed}
    ]

    board = [
      {0, 6},
      {6, 6},
      {6, 2},
      {2, 0},
      {0, 4},
      {4, 3},
      {3, 5},
      {5, 2},
      {2, 3},
      {3, 3},
      {3, 0}
    ]

    dominoes = []
    the_game = %Game{players: players, board: board, dominoes: dominoes, status: :started}

    game_over = GameOperations.is_game_over_by_closed_game?(the_game)

    assert game_over
  end

  test "game over by closed game is false because a player has valid dominoes at least" do
    players = [
      {%Player{
         dominoes: [{0, 3}, {4, 4}],
         nick_name: "player3",
         played_dominoes: [{2, 5}, {0, 4}, {0, 6}, {0, 2}, {0, 5}]
       }, :confirmed},
      {%Player{
         dominoes: [{1, 2}, {1, 3}, {1, 4}, {3, 6}],
         nick_name: "player2",
         played_dominoes: [{2, 3}, {3, 4}, {4, 5}]
       }, :confirmed},
      {%Player{
         dominoes: [{2, 6}, {5, 6}, {1, 6}, {1, 5}],
         nick_name: "player1",
         played_dominoes: [{3, 3}, {3, 5}, {5, 5}]
       }, :confirmed}
    ]

    board = [{0, 6}, {6, 6}, {6, 2}, {2, 0}, {0, 4}, {4, 3}, {3, 5}, {5, 2}, {2, 3}, {3, 3}]
    dominoes = []
    the_game = %Game{players: players, board: board, dominoes: dominoes, status: :started}

    game_over = GameOperations.is_game_over_by_closed_game?(the_game)

    assert not game_over
  end

  test "game over by closed game is false because there are dominoes in well yet" do
    players = [
      {%Player{
         dominoes: [{4, 4}],
         nick_name: "player3",
         played_dominoes: [{0, 3}, {2, 5}, {0, 4}, {0, 6}, {0, 2}, {0, 5}]
       }, :confirmed},
      {%Player{
         dominoes: [{1, 2}, {1, 3}, {1, 4}, {3, 6}],
         nick_name: "player2",
         played_dominoes: [{2, 3}, {3, 4}, {4, 5}]
       }, :confirmed},
      {%Player{
         dominoes: [{2, 6}, {5, 6}, {1, 6}, {1, 5}],
         nick_name: "player1",
         played_dominoes: [{3, 3}, {3, 5}, {5, 5}]
       }, :confirmed}
    ]

    board = [
      {0, 6},
      {6, 6},
      {6, 2},
      {2, 0},
      {0, 4},
      {4, 3},
      {3, 5},
      {5, 2},
      {2, 3},
      {3, 3},
      {3, 0}
    ]

    dominoes = [{0, 4}]
    the_game = %Game{players: players, board: board, dominoes: dominoes, status: :started}

    game_over = GameOperations.is_game_over_by_closed_game?(the_game)

    assert not game_over
  end

  test "finish game because it is closed" do
    players = [
      {%Player{
         dominoes: [{4, 4}],
         nick_name: "player3",
         played_dominoes: [{0, 3}, {2, 5}, {0, 4}, {0, 6}, {0, 2}, {0, 5}]
       }, :confirmed},
      {%Player{
         dominoes: [{1, 2}, {1, 3}, {1, 4}, {3, 6}],
         nick_name: "player2",
         played_dominoes: [{2, 3}, {3, 4}, {4, 5}]
       }, :confirmed},
      {%Player{
         dominoes: [{2, 6}, {5, 6}, {1, 6}, {1, 5}],
         nick_name: "player1",
         played_dominoes: [{3, 3}, {3, 5}, {5, 5}]
       }, :confirmed}
    ]

    board = [
      {0, 6},
      {6, 6},
      {6, 2},
      {2, 0},
      {0, 4},
      {4, 3},
      {3, 5},
      {5, 2},
      {2, 3},
      {3, 3},
      {3, 0}
    ]

    dominoes = []
    the_game = %Game{players: players, board: board, dominoes: dominoes, status: :started}

    {:ok, game_over} = GameOperations.finish_game(the_game)

    assert game_over.status == :finished
  end

  test "get players positions" do
    players = [
      {%Player{
         dominoes: [{1, 2}, {1, 3}, {1, 4}, {3, 6}],
         nick_name: "player2",
         played_dominoes: [{2, 3}, {3, 4}, {4, 5}]
       }, :confirmed},
      {%Player{
         dominoes: [{4, 4}],
         nick_name: "player3",
         played_dominoes: [{0, 3}, {2, 5}, {0, 4}, {0, 6}, {0, 2}, {0, 5}]
       }, :confirmed},
      {%Player{
         dominoes: [{2, 6}, {5, 6}, {1, 6}, {1, 5}],
         nick_name: "player1",
         played_dominoes: [{3, 3}, {3, 5}, {5, 5}]
       }, :confirmed}
    ]

    board = [
      {0, 6},
      {6, 6},
      {6, 2},
      {2, 0},
      {0, 4},
      {4, 3},
      {3, 5},
      {5, 2},
      {2, 3},
      {3, 3},
      {3, 0}
    ]

    dominoes = []
    the_game = %Game{players: players, board: board, dominoes: dominoes, status: :started}

    sort_players_by_positions = GameOperations.get_sort_players_positions(the_game)

    assert List.first(sort_players_by_positions).nick_name == "player3"
    assert List.last(sort_players_by_positions).nick_name == "player1"

    assert Enum.map(sort_players_by_positions, fn player -> player.nick_name end) == [
             "player3",
             "player2",
             "player1"
           ]
  end

  test "get players positions when some has domino 0,0" do
    players = [
      {%Player{
         dominoes: [{1, 2}],
         nick_name: "player2",
         played_dominoes: [{2, 3}, {3, 4}, {4, 5}, {1, 3}, {1, 4}, {3, 6}]
       }, :confirmed},
      {%Player{
         dominoes: [{0, 0}],
         nick_name: "player3",
         played_dominoes: [{0, 3}, {2, 5}, {0, 4}, {0, 6}, {0, 2}, {0, 5}]
       }, :confirmed},
      {%Player{
         dominoes: [],
         nick_name: "player1",
         played_dominoes: [{3, 3}, {3, 5}, {5, 5}, {2, 6}, {5, 6}, {1, 6}, {1, 5}]
       }, :confirmed}
    ]

    board = [
      {0, 6},
      {6, 6},
      {6, 2},
      {2, 0},
      {0, 4},
      {4, 3},
      {3, 5},
      {5, 2},
      {2, 3},
      {3, 3},
      {3, 0}
    ]

    dominoes = []
    the_game = %Game{players: players, board: board, dominoes: dominoes, status: :started}

    sort_players_by_positions = GameOperations.get_sort_players_positions(the_game)

    assert List.first(sort_players_by_positions).nick_name == "player1"
    assert List.last(sort_players_by_positions).nick_name == "player2"

    assert Enum.map(sort_players_by_positions, fn player -> player.nick_name end) == [
             "player1",
             "player3",
             "player2"
           ]
  end

  test "add rating to game for a player when he hasn't done yet" do
    players = [
      {%Player{nick_name: "player1"}, :confirmed},
      {%Player{nick_name: "player2"}, :confirmed},
      {%Player{nick_name: "player3"}, :confirmed}
    ]

    {:ok, the_game} = GameOperations.create()
    the_game = %Game{the_game | players: players, status: :finished}
    player = "player2"
    stars = 5

    {:ok, rating_game} = GameOperations.rating(the_game, player, stars)

    assert {player, stars} in rating_game.rating
  end

  test "update rating to game for a player when he already has done" do
    players = [
      {%Player{nick_name: "player1"}, :confirmed},
      {%Player{nick_name: "player2"}, :confirmed},
      {%Player{nick_name: "player3"}, :confirmed}
    ]

    {:ok, the_game} = GameOperations.create()

    the_game = %Game{
      the_game
      | players: players,
        status: :finished,
        rating: [{"player1", 5}, {"player2", 3}]
    }

    player = "player1"
    stars = 3

    {:ok, rating_game} = GameOperations.rating(the_game, player, stars)

    assert {player, stars} in rating_game.rating
    assert {player, 5} not in rating_game.rating
  end

  test "close game when all players are exit" do
    players = [
      {%Player{nick_name: "player1"}, :confirmed},
      {%Player{nick_name: "player2"}, :exit},
      {%Player{nick_name: "player3"}, :exit}
    ]

    {:ok, the_game} = GameOperations.create()
    the_game = %Game{the_game | players: players, status: :finished}

    {:ok, closed_game} = GameOperations.close(the_game, "player1")
    assert closed_game.status == :close
  end

  test "don't close game because there are players no exit" do
    players = [
      {%Player{nick_name: "player1"}, :confirmed},
      {%Player{nick_name: "player2"}, :confirmed},
      {%Player{nick_name: "player3"}, :exit}
    ]

    {:ok, the_game} = GameOperations.create()
    the_game = %Game{the_game | players: players, status: :finished}

    {:ok, closed_game} = GameOperations.close(the_game, "player1")
    assert closed_game.status != :close
  end
end
