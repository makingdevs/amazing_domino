const BoardManager = {
  showDominoes () {
    if ($("#status_game").val() == "started") {
      $("#player_dominoes").focus()
    }
    let increase_board = true
    this.show_ghost_domino_center()
    while (increase_board) {
      let initial_domino = this.getInitialDomino()
      let initial_position = this.showInitialDomino(initial_domino)
      let boardDominoesData = this.getBoardDominoesData(initial_domino)
      increase_board = this.showLeftDominoes(boardDominoesData.left_dominoes, initial_position, boardDominoesData.size_board)
      if (!increase_board) {
        increase_board = this.showRightDominoes(boardDominoesData.right_dominoes, initial_position, boardDominoesData.size_board)
      }
      if (increase_board) {
        this.hideAllDominoes(boardDominoesData.all_dominoes)
        this.increaseBoardHeight(boardDominoesData.size_board.height)
      }
    }
    if ($("#status_game").val() == "finished") {
      $("#star_rating").focus()
    }
  },

  show_ghost_domino_center() {
    let board_dominoes = this.getBoardDominoes()
    if (board_dominoes.length == 0) {
      let top = this.getTopPositionForDomino(90)
      let left = this.getLeftPositionForDomino(40)
      $("#ghost_domino_center").removeClass("domino")
      $("#ghost_domino_center").addClass("vertical-domino")
      $("#ghost_domino_center").css({
        top: top,
        left: left
       });
    }
  },

  hideAllDominoes(all_dominoes) {
    for (var domino of all_dominoes) {
      $("#domino" + domino.x + domino.y).css({
        visibility: 'hidden'
      });
      $("#inverse_domino" + domino.x + domino.y).css({
        visibility: 'hidden'
      });
    }
  },

  increaseBoardHeight(current_height) {
    current_height += 180
    $("#board").css({
      height: current_height
    });
  },

  getInitialDomino() {
    return {x: $("#initial_domino_x").val(), y: $("#initial_domino_y").val()}
  },

  getTopPositionForDomino(heightDomino) {
    var board = document.getElementById('board');
    var boardHeight = board.clientHeight;
    var centerH = Math.trunc(boardHeight / 2);
    var topDomino = centerH - (Math.trunc(heightDomino / 2));
    return topDomino;
  },

  getLeftPositionForDomino(widthDomino) {
    var board = document.getElementById('board');
    var boardWidth = board.clientWidth;
    var centerW = Math.trunc(boardWidth / 2);
    var leftDomino = centerW - (Math.trunc(widthDomino / 2));
    return leftDomino;
  },

  getHeightDomino(orientation, direction) {
    var heightDomino = 40;
    if (orientation === 'portrait') {
      heightDomino = 90;
    }
    return heightDomino;
  },

  getWidthDomino (orientation, direction) {
    var widthDomino = 90;
    if (orientation === 'portrait' && direction === 'row') {
      widthDomino = 40;
    }
    return widthDomino;
  },

  showInitialDomino (initial_domino) {
    var widthDomino = this.getWidthDomino('portrait', 'row')
    var heightDomino = this.getHeightDomino('portrait', 'row')
    this.addCssVerticalForDomino(initial_domino, "")
    if (initial_domino.x !== initial_domino.y) {
      this.removeCssVerticalForDomino(initial_domino, "")
      widthDomino = this.getWidthDomino('landscape', 'row')
      heightDomino = this.getHeightDomino('landscape', 'row')
    }
    var initial_position = {
      top: this.getTopPositionForDomino(heightDomino),
      left: this.getLeftPositionForDomino(widthDomino)
    }
    this.show_domino(initial_domino, initial_position.top, initial_position.left, "")
    this.show_ghost_domino_left(initial_position, "left", "portrait", false)
    this.show_ghost_domino_right(initial_position, "right", "portrait", false)
    return initial_position;
  },

  getBoardDominoes() {
    let total_dominoes = parseInt($("#total_dominoes").val())
    let board_dominoes = []
    for (let i =0; i < total_dominoes; i++) {
      board_dominoes.push({x: $("#domino_x_" + i).val(), y: $("#domino_y_" + i).val()})
    }
    return board_dominoes
  },

  getLeftDominoes(board_dominoes, initial_domino) {
    let left_dominoes = []
    let initial_index = board_dominoes.findIndex(domino => domino.x==initial_domino.x && domino.y==initial_domino.y)
    left_dominoes = board_dominoes.slice(0, initial_index).reverse()
    return left_dominoes
  },

  getRightDominoes(board_dominoes, initial_domino) {
    let right_dominoes = []
    let initial_index = board_dominoes.findIndex(domino => domino.x==initial_domino.x && domino.y==initial_domino.y)
    right_dominoes = board_dominoes.slice(initial_index + 1, board_dominoes.length)
    return right_dominoes
  },

  getBoardDominoesData(initial_domino) {
    let board_dominoes = this.getBoardDominoes()

    let board = document.getElementById('board')
    let size_board = {width: board.clientWidth, height: board.clientHeight}

    return {
      all_dominoes: board_dominoes,
      initial_domino: initial_domino,
      left_dominoes: this.getLeftDominoes(board_dominoes, initial_domino),
      right_dominoes: this.getRightDominoes(board_dominoes, initial_domino),
      size_board: size_board
    }
  },

  getNewPositionDominoToLeft(domino, current_position, orientation_before, change_direction, valign = "top") {
    let topDomino = current_position.top
    let leftDomino = current_position.left
    if (!change_direction) {
      let widthDomino = this.get_width_for_current_domino(domino, "row")
      topDomino = orientation_before == "portrait" ? topDomino + 22
        : (domino.x == domino.y) ? topDomino - 22 : topDomino
      leftDomino -= (widthDomino + 1)
    } else {
      leftDomino -= 91
      topDomino = valign == "top" ? topDomino : topDomino + 50
    }
    return {top: topDomino, left: leftDomino}
  },

  getNewPositionDominoToUp(domino, current_position, orientation_before, flow) {
    let topDomino = current_position.top
    let leftDomino = current_position.left
    topDomino -= 91
    if (flow === "right") {
      leftDomino = orientation_before == "portrait" ? leftDomino
        : leftDomino + 50
    }
    return {top: topDomino, left: leftDomino}
  },

  getNewPositionDominoToDown(domino, current_position, orientation_before, flow) {
    let topDomino = current_position.top
    let leftDomino = current_position.left
    topDomino = orientation_before == "portrait" ? topDomino + 91
      : topDomino + 41
    if (flow === "right") {
      leftDomino = orientation_before == "portrait" ? leftDomino
        : leftDomino + 50
    }
    return {top: topDomino, left: leftDomino}
  },

  getNewPositionDominoToRight(domino, current_position, orientation_before, change_direction, valign = "top") {
    let topDomino = current_position.top
    let leftDomino = current_position.left
    if (!change_direction) {
      topDomino = orientation_before == "portrait" ? topDomino + 22
      : (domino.x == domino.y) ? topDomino - 22 : topDomino
    } else {
      topDomino = valign == "top" ? topDomino : topDomino + 50
    }
    leftDomino = orientation_before == "portrait" ? leftDomino + 41 : leftDomino + 91
    return {top: topDomino, left: leftDomino}
  },

  addCssVerticalForDomino(domino, inverse) {
    inverse = inverse !== "" ? inverse + "_" : ""
    $("#" + inverse + "domino" + domino.x + domino.y).removeClass("domino")
    $("#" + inverse + "domino" + domino.x + domino.y).addClass("vertical-domino")
  },

  removeCssVerticalForDomino(domino, inverse) {
    inverse = inverse !== "" ? inverse + "_" : ""
    $("#" + inverse + "domino" + domino.x + domino.y).addClass("domino")
    $("#" + inverse + "domino" + domino.x + domino.y).removeClass("vertical-domino")
  },

  showLeftDominoes(left_dominoes, previous_position, size_board) {
    let current_position = previous_position
    let inverse = ""
    let flow = "left"
    let boundary = 0
    let orientation_before = "portrait"
    let change_direction = false
    let increase_height = false
    let index = 0
    for (var domino of left_dominoes) {
      if (increase_height) {
        break
      }
      let new_position = {}
      switch (flow) {
        case "left":
          boundary = 25
          new_position = this.getNewPositionDominoToLeft(domino, current_position, orientation_before, change_direction)
          if (change_direction) {
            this.removeCssVerticalForDomino(domino, inverse)
            orientation_before = "landscape"
            change_direction = false
          } else {
            if (new_position.left > boundary) {
              if (domino.x == domino.y) {
                this.addCssVerticalForDomino(domino, inverse)
                orientation_before = "portrait"
              } else {
                this.removeCssVerticalForDomino(domino, inverse)
                orientation_before = "landscape"
              }
            } else {
              new_position = this.getNewPositionDominoToUp(domino, current_position, orientation_before, flow)
              this.addCssVerticalForDomino(domino, inverse)
              orientation_before = "portrait"
              flow = "right"
              change_direction = true
              if (new_position.top < 25) {
                increase_height = true
              }
            }
          }
          break
        case "right":
          boundary = size_board.width - 25
          inverse = "inverse"
          new_position = this.getNewPositionDominoToRight(domino, current_position, orientation_before, change_direction)
          if (change_direction) {
            this.removeCssVerticalForDomino(domino, inverse)
            orientation_before = "landscape"
            change_direction = false
          } else {
            let widthDomino = this.get_width_for_current_domino(domino, "row")
            if ((new_position.left + widthDomino + 1) < boundary) {
              if (domino.x == domino.y) {
                this.addCssVerticalForDomino(domino, inverse)
                orientation_before = "portrait"
              } else {
                this.removeCssVerticalForDomino(domino, inverse)
                orientation_before = "landscape"
              }
            } else {
              new_position = this.getNewPositionDominoToUp(domino, current_position, orientation_before, flow)
              orientation_before = "portrait"
              flow = "left"
              inverse = ""
              this.addCssVerticalForDomino(domino, inverse)
              change_direction = true
              if (new_position.top < 25) {
                increase_height = true
              }
            }
          }
          break
      }
      if (!increase_height) {
        this.show_domino(domino, new_position.top, new_position.left, inverse)
        current_position = new_position
        if (index == left_dominoes.length - 1) {
          this.show_ghost_domino_left(current_position, flow, orientation_before, change_direction)
         }
        index++;
      }
    }
    return increase_height
  },

  showRightDominoes(right_dominoes, previous_position, size_board) {
    let current_position = previous_position
    let inverse = ""
    let flow = "right"
    let boundary = 0
    let orientation_before = "portrait"
    let change_direction = false
    let increase_height = false
    let index = 0
    for (var domino of right_dominoes) {
      if (increase_height) {
        break
      }
      let new_position = {}
      switch (flow) {
        case "right":
          boundary = size_board.width - 25
          new_position = this.getNewPositionDominoToRight(domino, current_position, orientation_before, change_direction, "down")
          if (change_direction) {
            this.removeCssVerticalForDomino(domino, inverse)
            orientation_before = "landscape"
            change_direction = false
          } else {
            let widthDomino = this.get_width_for_current_domino(domino, "row")
            if ((new_position.left + widthDomino + 1) < boundary) {
              if (domino.x == domino.y) {
                this.addCssVerticalForDomino(domino, inverse)
                orientation_before = "portrait"
              } else {
                this.removeCssVerticalForDomino(domino, inverse)
                orientation_before = "landscape"
              }
            } else {
              new_position = this.getNewPositionDominoToDown(domino, current_position, orientation_before, flow)
              orientation_before = "portrait"
              flow = "left"
              inverse = ""
              this.addCssVerticalForDomino(domino, inverse)
              change_direction = true
              if ((new_position.top + 91) > (size_board.height - 25)) {
                increase_height = true
              }
            }
          }
          break
        case "left":
          boundary = 25
          inverse = "inverse"
          new_position = this.getNewPositionDominoToLeft(domino, current_position, orientation_before, change_direction, "down")
          if (change_direction) {
            this.removeCssVerticalForDomino(domino, inverse)
            orientation_before = "landscape"
            change_direction = false
          } else {
            if (new_position.left > boundary) {
              if (domino.x == domino.y) {
                this.addCssVerticalForDomino(domino, inverse)
                orientation_before = "portrait"
              } else {
                this.removeCssVerticalForDomino(domino, inverse)
                orientation_before = "landscape"
              }
            } else {
              inverse = ""
              new_position = this.getNewPositionDominoToDown(domino, current_position, orientation_before, flow)
              this.addCssVerticalForDomino(domino, inverse)
              orientation_before = "portrait"
              flow = "right"
              change_direction = true
              if ((new_position.top + 91) > (size_board.height - 25)) {
                increase_height = true
              }
            }
          }
          break
      }
      if (!increase_height) {
        this.show_domino(domino, new_position.top, new_position.left, inverse)
        current_position = new_position
        if (index == right_dominoes.length - 1) {
          this.show_ghost_domino_right(current_position, flow, orientation_before, change_direction)
        }
        index++
      }
    }
    return increase_height
  },

  get_height_for_current_domino(domino, direction) {
    return domino.x === domino.y ? this.getHeightDomino('landscape', direction)
      : this.getHeightDomino('portrait', direction)
  },

  get_width_for_current_domino(domino, direction) {
    return domino.x === domino.y ? this.getWidthDomino('portrait', direction)
      : this.getWidthDomino('landscape', direction)
  },

  show_domino(domino, top, left, inverse = "") {
    inverse = inverse !== "" ? inverse + "_" : inverse
    $("#"+ inverse + "domino" + domino.x + domino.y).css({
      visibility: 'visible',
      top: top,
      left: left
    });
  },

  show_ghost_domino_left(current_position, flow, orientation_before, change_direction) {
    let width = 91
    let board = document.getElementById('board');
    let new_left = flow == "left"
      ? current_position.left - width
      : orientation_before == "portrait"
        ? current_position.left + 41
        : current_position.left + width
    let new_top = orientation_before == "portrait" && !change_direction ? current_position.top + 22 : current_position.top

    if (flow == "left" && new_left < 25) {
      new_left = current_position.left
      new_top = current_position.top - 91
      $("#ghost_domino_left").removeClass("domino")
      $("#ghost_domino_left").addClass("vertical-domino")
    }

    if (flow == "right" && new_left + 91 > board.clientWidth - 25) {
      new_left = orientation_before == "portrait" ? current_position.left : current_position.left + 50
      new_top = current_position.top - 91
      $("#ghost_domino_left").removeClass("domino")
      $("#ghost_domino_left").addClass("vertical-domino")
    }

    $("#ghost_domino_left").css({
      top: new_top,
      left: new_left
     });
  },

  show_ghost_domino_right(current_position, flow, orientation_before, change_direction) {
    let width = orientation_before == "portrait" ? 41 : 91
    let board = document.getElementById('board');
    let new_top = orientation_before == "portrait" ? change_direction ? current_position.top + 50 : current_position.top + 22 : current_position.top
    let new_left = flow == "right"
      ? current_position.left + width
      : current_position.left - 91

    if (flow == "right" && new_left + 91 > board.clientWidth - 25) {
      new_left = orientation_before == "portrait" ? current_position.left : current_position.left + 50
      new_top = orientation_before == "portrait" ? current_position.top + 91 : current_position.top + 41
      $("#ghost_domino_right").removeClass("domino")
      $("#ghost_domino_right").addClass("vertical-domino")
    }

    if (flow == "left" && new_left < 25) {
      new_left = current_position.left
      new_top = orientation_before == "portrait" ? current_position.top + 91 : current_position.top + 41
      $("#ghost_domino_right").removeClass("domino")
      $("#ghost_domino_right").addClass("vertical-domino")
    }

    $("#ghost_domino_right").css({
      top: new_top,
      left: new_left
     });
  }

}
export default BoardManager
