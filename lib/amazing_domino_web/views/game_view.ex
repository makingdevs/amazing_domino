defmodule AmazingDominoWeb.GameView do
  use AmazingDominoWeb, :view
  alias AmazingDomino.Engine.GameOperations
  alias AmazingDomino.Engine.GameOperations.Game

  def current_turn_for_user?(game, user) do
    player_in_turn = GameOperations.get_player_in_turn(game)
    user == player_in_turn.nick_name
  end

  def is_selected_domino?(data) do
    not is_nil(data.data_play.selected_domino)
  end

  def current_user_is_pending?(game, user) do
    game.players
    |> Enum.filter(fn {_, status} -> status == :pending end)
    |> Enum.map(fn {player, _status} -> player.nick_name end)
    |> Enum.member?(user)
  end

  def current_user_is_confirmed?(game, user) do
    game.players
    |> Enum.filter(fn {_, status} -> status == :confirmed end)
    |> Enum.map(fn {player, _status} -> player.nick_name end)
    |> Enum.member?(user)
  end

  def all_players_are_confirmed?(game) do
    game.players
    |> Enum.count(fn {_, status} -> status == :pending end) == 0
  end

  def start_action_in_game(game) do
    :start in game.actions
  end

  def user_is_in_game?(game, user) do
    game.players
    |> Enum.map(fn {player, _status} -> player.nick_name end)
    |> Enum.member?(user)
  end

  def game_start_before_user_join_in?(game, user) do
    not is_nil(game) and game.status != :created and
      not user_is_in_game?(game, user)
  end

  def get_dominoes_for_current_user(game, user) do
    current_player =
      game.players
      |> Enum.map(fn {player, _status} -> player end)
      |> Enum.find(fn player -> player.nick_name == user end)

    current_player.dominoes
  end

  def is_valid_domino_to_play?(game, domino) do
    game.valid_dominoes_to_play
    |> Enum.member?(domino)
  end

  def get_index_for_valid_domino(game, domino) do
    game.valid_dominoes_to_play
    |> Enum.find_index(&(&1 == domino))
  end

  def is_valid_domino_to_play_selected?(game, data, domino) do
    not is_nil(data.data_play.selected_domino) &&
      String.to_integer(data.data_play.selected_domino) ==
        get_index_for_valid_domino(game, domino)
  end

  def get_players_without_current_user(game, user) do
    game.players
    |> Enum.filter(fn {player, _status} -> player.nick_name != user end)
  end

  def is_current_turn_for_player?(game, player) do
    current_turn_player =
      game.players
      |> Enum.map(fn {player, _status} -> player end)
      |> Enum.at(game.current_turn)

    current_turn_player.nick_name == player.nick_name
  end

  def is_suggest_board_domino?(data, domino) do
    not is_nil(data.data_play.suggested_board_dominoes) &&
      Enum.member?(Map.values(data.data_play.suggested_board_dominoes), domino)
  end

  def is_suggested_domino_in_board_for_side?(data, {vx, vy}, the_side) do
    with {side, _} <-
           Enum.find(
             data.data_play.suggested_board_dominoes,
             fn {side, {x, y}} -> side == the_side && x == vx && y == vy end
           ),
         do: side == the_side
  end

  def is_checked_star(current_rating, star) do
    if current_rating >= star, do: "checked", else: "unchecked"
  end

  def get_star_rating_for_user(game, user) do
    case Enum.find(game.rating, fn {player, _} -> player == user end) do
      nil -> 0
      {_player, stars} -> stars
    end
  end

  def player_exit_game?(game, user) do
    player =
      game.players
      |> Enum.find(fn {player, _status} -> player.nick_name == user end)

    case player do
      {_player, status} -> status == :exit
      _ -> false
    end
  end

  def color_for_games_record(percentage) do
    percentage = String.to_float(percentage)

    cond do
      percentage >= 0.0 && percentage < 40 -> "red lighten-3"
      percentage >= 40 && percentage < 60 -> "orange lighten-2"
      percentage >= 60 && percentage < 80 -> "yellow darken-1"
      percentage >= 80 && percentage < 100 -> "light-blue lighten-3"
      percentage == 100 -> "light-green accent-4"
      true -> ""
    end
  end

  def icon_for_games_record(percentage) do
    percentage = String.to_float(percentage)

    cond do
      percentage >= 0.0 && percentage < 40 ->
        "fas fa-dove fa-2x red-text text-lighten-3"

      percentage >= 40 && percentage < 60 ->
        "fas fa-kiwi-bird fa-2x orange-text text-lighten-2"

      percentage >= 60 && percentage < 80 ->
        "fas fa-crow fa-2x yellow-text text-darken-1"

      percentage >= 80 && percentage < 100 ->
        "fas fa-horse-head fa-2x light-blue-text text-lighten-"

      percentage == 100 ->
        "fas fa-dragon fa-2x light-green-text text-accent-4"

      true ->
        ""
    end
  end

  def new_game_player_info(%Game{players: [{p, :confirmed} | _]}) do
    p.nick_name
  end
end
