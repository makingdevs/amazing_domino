defmodule AmazingDominoWeb.UserSessionView do
  use AmazingDominoWeb, :view

  def suggest_nickname(conn, _opts \\ %{}) do
    if user = conn.assigns[:user] do
      user.nick
    else
      "domino_#{:rand.uniform(100_000)}"
    end
  end
end
