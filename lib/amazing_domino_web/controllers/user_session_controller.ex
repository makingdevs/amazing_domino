defmodule AmazingDominoWeb.UserSessionController do
  use AmazingDominoWeb, :controller
  alias AmazingDominoWeb.UserAuth

  def index(conn, _params) do
    render(conn, "index.html", token: get_csrf_token())
  end

  def login(conn, params) do
    if params["g-recaptcha-response"] == "" do
      conn =
        put_flash(
          conn,
          :error,
          "Este juego es sólo para humanos, los robots no están autorizados !!!"
        )

      redirect(conn, to: "/")
    else
      case Recaptcha.verify(params["g-recaptcha-response"]) do
        {:ok, _response} ->
          user = %{nick: params["username"], avatar: nil, email: nil}
          UserAuth.login(conn, user)

        {:error, errors} ->
          conn = put_flash(conn, :error, errors)
          redirect(conn, to: "/")
      end
    end
  end

  def login_fb(conn, params) do
    user = %{nick: params["name_fb"], avatar: params["avatar_fb"], email: params["email_fb"]}
    UserAuth.login(conn, user)
  end

  def logout(conn, _params) do
    UserAuth.logout(conn)
  end
end
