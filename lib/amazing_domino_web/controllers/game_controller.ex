defmodule AmazingDominoWeb.GameController do
  use AmazingDominoWeb, :controller
  alias AmazingDomino.GameManager

  def home(conn, _params) do
    render(conn, "index.html", game: nil)
  end

  def create(conn, _params) do
    game = GameManager.create()
    user = get_session(conn, :user)
    {game, _user, _error} = GameManager.add_player(game.uuid, {user, :confirmed})
    redirect(conn, external: "/amazing/#{game.uuid}")
  end

  def join(conn, params) do
    user = get_session(conn, :user)
    {game, _user, error} = GameManager.join(params["game_uuid"], user)

    if not is_nil(error) do
      conn
      |> put_flash(:error, error)
      |> render("index.html", game: game)
    else
      redirect(conn, external: "/amazing/#{game.uuid}")
    end
  end
end
