defmodule AmazingDominoWeb.Router do
  use AmazingDominoWeb, :router

  import AmazingDominoWeb.UserAuth

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_live_flash)
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
    plug(:fetch_current_user)
    plug(:put_root_layout, {AmazingDominoWeb.LayoutView, :root})
  end

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/", AmazingDominoWeb do
    pipe_through([:browser, :redirect_if_user_is_authenticated])
    get("/", UserSessionController, :index)
    post("/", UserSessionController, :login)
    post("/fb", UserSessionController, :login_fb)
  end

  scope "/logout", AmazingDominoWeb do
    pipe_through([:browser])
    get("/", UserSessionController, :logout)
  end

  scope "/", AmazingDominoWeb do
    pipe_through([:browser, :require_authenticated_user])
    get("/home", GameController, :home)
    get("/create", GameController, :create)
    get("/join", GameController, :join)
  end

  scope "/amazing", AmazingDominoWeb do
    pipe_through([:browser, :require_authenticated_user])

    live("/", GameLive)
    live("/:uuid", GameLive)
    # live "/create", GameLiveController
  end

  # Other scopes may use custom stacks.
  # scope "/api", AmazingDominoWeb do
  #   pipe_through :api
  # end
end
