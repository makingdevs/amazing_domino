defmodule AmazingDominoWeb.UserAuth do
  import Plug.Conn
  import Phoenix.Controller
  alias AmazingDominoWeb.Router.Helpers, as: Routes
  alias AmazingDomino.GameManager

  def fetch_current_user(conn, _opts) do
    user = get_session(conn, :user)

    if user do
      user = if :avatar not in Map.keys(user), do: Map.put(user, :avatar, nil), else: user
      user = if :email not in Map.keys(user), do: Map.put(user, :email, nil), else: user
      record = GameManager.games_record_for_player(user.nick)

      conn
      |> put_session(:user, user)
      |> assign(:user, user)
      |> assign(:record, record)
    else
      conn
    end
  end

  def redirect_if_user_is_authenticated(conn, _opts) do
    user = get_session(conn, :user)

    if user do
      user = if :avatar not in Map.keys(user), do: Map.put(user, :avatar, nil), else: user
      user = if :email not in Map.keys(user), do: Map.put(user, :email, nil), else: user
      record = GameManager.games_record_for_player(user.nick)

      conn
      |> put_session(:user, user)
      |> put_session(:record, record)
      |> redirect(to: Routes.game_path(conn, :home))
      |> halt()
    else
      conn
    end
  end

  def require_authenticated_user(conn, _opts) do
    if conn.assigns[:user] do
      conn
    else
      conn
      |> put_flash(:info, "Debes ponerte un nombre...")
      |> maybe_store_return_to()
      |> redirect(to: Routes.user_session_path(conn, :index))
      |> halt()
    end
  end

  def login(conn, user, _params \\ %{}) do
    user_return_to = get_session(conn, :user_return_to)
    record = GameManager.games_record_for_player(user.nick)

    conn
    |> configure_session(renew: true)
    |> clear_session()
    |> put_session(:user, user)
    |> put_session(:record, record)
    |> redirect(to: user_return_to || Routes.game_path(conn, :home))
  end

  def logout(conn, _params \\ %{}) do
    conn
    |> configure_session(renew: true)
    |> clear_session()
    |> redirect(to: Routes.user_session_path(conn, :index))
  end

  defp maybe_store_return_to(
         %{method: "GET", request_path: request_path, query_string: query_string} = conn
       ) do
    put_session(conn, :user_return_to, request_path <> "?" <> query_string)
  end

  defp maybe_store_return_to(conn), do: conn
end
