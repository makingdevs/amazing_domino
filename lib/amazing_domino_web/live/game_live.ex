defmodule AmazingDominoWeb.GameLive do
  use Phoenix.LiveView, layout: {AmazingDominoWeb.LayoutView, "live.html"}

  alias AmazingDomino.GameManager
  alias AmazingDomino.Engine.GameOperations.Game

  def render(assigns) do
    AmazingDominoWeb.GameView.render("game.html", assigns)
  end

  def mount(%{"uuid" => uuid}, session, socket) do
    AmazingDominoWeb.Endpoint.subscribe(uuid)
    user = session["user"]
    # TODO: Use the full user info
    socket = assign(socket, current_user: user)

    data =
      case Map.has_key?(socket.assigns, :data) do
        true ->
          socket.assigns.data

        false ->
          %{
            buttons_status: %{play: "hidden"},
            new_game: %Game{},
            data_play: %{
              selected_domino: nil,
              selected_side: nil,
              board_clicked_domino: nil,
              suggested_board_dominoes: nil
            }
          }
      end

    game =
      case GameManager.get_game(uuid) do
        {_, game} -> game
        _ -> nil
      end

    socket = assign(socket, game: game, data: data, user: user.nick, avatar: user.avatar)
    {:ok, socket}
  end

  def handle_event("confirm_join", details, socket) do
    {game, user, _error} = GameManager.confirm_join(details["uuid"], details["player"])
    data = socket.assigns.data
    buttons_status = Map.put(data.buttons_status, :play, "hidden")
    data = Map.put(data, :buttons_status, buttons_status)

    data_play = %{
      selected_domino: nil,
      selected_side: nil,
      board_clicked_domino: nil,
      suggested_board_dominoes: nil
    }

    data = Map.put(data, :data_play, data_play)
    socket = assign(socket, game: game, data: data, user: user)
    AmazingDominoWeb.Endpoint.broadcast_from(self(), game.uuid, "confirm_join", socket.assigns)
    {:noreply, socket}
  end

  def handle_event("cancel_join", details, socket) do
    {game, user, _error} = GameManager.cancel_join(details["uuid"], details["player"])
    data = socket.assigns.data

    data_play = %{
      selected_domino: nil,
      selected_side: nil,
      board_clicked_domino: nil,
      suggested_board_dominoes: nil
    }

    data = Map.put(data, :data_play, data_play)
    socket = assign(socket, game: game, data: data, user: user)
    AmazingDominoWeb.Endpoint.broadcast_from(self(), game.uuid, "cancel_join", socket.assigns)
    {:noreply, socket}
  end

  def handle_event("start", details, socket) do
    {game, user, _error} = GameManager.start(details["uuid"], details["player"])
    data = socket.assigns.data
    buttons_status = Map.put(data.buttons_status, :play, "hidden")
    data = Map.put(data, :buttons_status, buttons_status)

    data_play = %{
      selected_domino: nil,
      selected_side: nil,
      board_clicked_domino: nil,
      suggested_board_dominoes: nil
    }

    data = Map.put(data, :data_play, data_play)

    socket =
      socket
      |> update(:game, fn _state -> game end)
      |> update(:user, fn _state -> user end)
      |> update(:data, fn _state -> data end)

    AmazingDominoWeb.Endpoint.broadcast_from(self(), game.uuid, "start", socket.assigns)
    {:noreply, socket}
  end

  def handle_event("selected_domino", details, socket) do
    {game, _, _} = GameManager.refresh(details["uuid"], details["player"])
    data = socket.assigns.data

    # Elije el lado derecho cuando solo hay una ficha.
    selected_side =
      case is_nil(List.first(game.board)) do
        true -> "right"
        false -> nil
      end

    data_play = Map.put(data.data_play, :selected_side, selected_side)
    data_play = Map.put(data_play, :selected_domino, details["index_selected_domino"])

    suggest_board_dominoes =
      get_suggest_dominoes(
        List.first(game.board),
        List.last(game.board),
        Enum.at(game.valid_dominoes_to_play, String.to_integer(data_play.selected_domino))
      )

    data_play = Map.put(data_play, :suggested_board_dominoes, suggest_board_dominoes)

    data = Map.put(data, :data_play, data_play)

    buttons_status = socket.assigns.data.buttons_status

    buttons_status =
      case not is_nil(data_play.selected_domino) and
             not is_nil(data_play.selected_side) do
        true -> Map.put(buttons_status, :play, "")
        false -> Map.put(buttons_status, :play, "hidden")
      end

    data = Map.put(data, :buttons_status, buttons_status)

    {:noreply, assign(socket, data: data)}
  end

  def handle_event("make_a_play", details, socket) do
    data = socket.assigns.data
    data_play = data.data_play

    board_clicked_domino =
      case not is_nil(details["face_a"]) do
        true -> {String.to_integer(details["face_a"]), String.to_integer(details["face_b"])}
        false -> nil
      end

    data_play = Map.put(data_play, :board_clicked_domino, board_clicked_domino)

    selected_side = details["selected_side"]
    data_play = Map.put(data_play, :selected_side, to_string(selected_side))

    {game, user, error} =
      GameManager.make_a_play(
        details["uuid"],
        details["player"],
        data_play.selected_domino,
        data_play.selected_side
      )

    if game.status != :finished do
      {_game, _user, _error} = GameManager.change_turn(details["uuid"], details["player"])
    end

    {_, new_game} = GameManager.get_game(details["uuid"])

    data_play = %{
      selected_domino: nil,
      selected_side: nil,
      board_clicked_domino: nil,
      suggested_board_dominoes: nil
    }

    data = Map.put(data, :data_play, data_play)

    socket =
      socket
      |> update(:game, fn _state -> new_game end)
      |> update(:user, fn _state -> user end)
      |> update(:data, fn _state -> data end)

    socket = put_flash(socket, :error, error)

    AmazingDominoWeb.Endpoint.broadcast_from(self(), game.uuid, "make_a_play", socket.assigns)
    {:noreply, socket}
  end

  def handle_event("change_turn", details, socket) do
    {game, user, _error} = GameManager.change_turn(details["uuid"], details["player"])
    data = socket.assigns.data
    buttons_status = Map.put(data.buttons_status, :play, "hidden")
    data = Map.put(data, :buttons_status, buttons_status)

    data_play = %{
      selected_domino: nil,
      selected_side: nil,
      board_clicked_domino: nil,
      suggested_board_dominoes: nil
    }

    data = Map.put(data, :data_play, data_play)

    socket =
      socket
      |> update(:game, fn _state -> game end)
      |> update(:user, fn _state -> user end)
      |> update(:data, fn _state -> data end)

    AmazingDominoWeb.Endpoint.broadcast_from(self(), game.uuid, "make_a_play", socket.assigns)
    {:noreply, socket}
  end

  def handle_event("take_from_well", details, socket) do
    {game, user, _error} = GameManager.take_from_well(details["uuid"], details["player"])
    data = socket.assigns.data

    socket =
      socket
      |> update(:game, fn _state -> game end)
      |> update(:user, fn _state -> user end)
      |> update(:data, fn _state -> data end)

    AmazingDominoWeb.Endpoint.broadcast_from(self(), game.uuid, "take_from_well", socket.assigns)
    {:noreply, socket}
  end

  def handle_event("close", details, socket) do
    {game, user, error} = GameManager.close(details["uuid"], details["player"])

    socket =
      socket
      |> update(:game, fn _state -> game end)
      |> update(:user, fn _state -> user end)

    socket = put_flash(socket, :error, error)
    {:noreply, socket}
  end

  def handle_event("restart", details, socket) do
    {_game, _user, _error} = GameManager.close(details["uuid"], details["player"])

    new_game = GameManager.create()
    user = socket.assigns.current_user
    {new_game, _user, _error} = GameManager.add_player(new_game.uuid, {user, :confirmed})

    data =
      socket.assigns.data
      |> Map.put(:new_game, new_game)

    socket =
      socket
      |> update(:data, fn _state -> data end)

    AmazingDominoWeb.Endpoint.broadcast_from(
      self(),
      socket.assigns.game.uuid,
      "restart",
      socket.assigns
    )

    {:noreply, redirect(socket, external: "/amazing/#{new_game.uuid}")}
  end

  def handle_event("rating", details, socket) do
    {game, _user, _error} =
      GameManager.rate_game(details["uuid"], details["player"], details["stars"])

    socket =
      socket
      |> update(:game, fn _state -> game end)
      |> update(:user, fn _state -> details["player"] end)

    {:noreply, socket}
  end

  def handle_info(msg, socket) do
    {:noreply, assign(socket, game: msg.payload.game, data: msg.payload.data)}
  end

  defp get_suggest_dominoes(nil, nil, selected), do: selected

  defp get_suggest_dominoes({a, b}, {c, d}, {x, y}) do
    cond do
      (x == a or y == a) and (x == d or y == d) -> %{left: {a, b}, right: {c, d}}
      x == a or y == a -> %{left: {a, b}}
      x == d or y == d -> %{right: {c, d}}
    end
  end
end
