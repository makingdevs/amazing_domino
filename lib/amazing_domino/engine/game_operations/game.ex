defmodule AmazingDomino.Engine.GameOperations.Game do
  defstruct uuid: "",
            status: :off,
            players: [],
            dominoes: [],
            current_turn: 0,
            board: [],
            initial_domino: nil,
            actions: [],
            valid_dominoes_to_play: [],
            positions: [],
            winner: nil,
            rating: []
end
