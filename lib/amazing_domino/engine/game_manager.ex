defmodule AmazingDomino.GameManager do
  alias AmazingDomino.Engine.GameOperations
  alias AmazingDomino.Engine.PlayerOperations
  alias AmazingDomino.Engine.PlayerOperations.Player
  alias AmazingDomino.Repository.PlayerRepository
  alias AmazingDomino.Repository.GameRepository

  def create() do
    {:ok, game} = GameOperations.create()

    DynamicSupervisor.start_child(
      AmazingDomino.DynamicSupervisor,
      {Agent, fn -> game end}
    )

    game
  end

  def add_player(uuid, {user, status}) do
    {agent_game, game} = uuid |> get_game
    player = PlayerOperations.create(user)

    case PlayerRepository.player_existing(user.nick) do
      nil ->
        do_add_player(
          {agent_game, game},
          {player, status},
          PlayerRepository.valid_player?(user.nick)
        )

      uuid ->
        player = %Player{player | uuid: uuid}
        do_add_player({agent_game, game}, {player, status}, true)
    end
  end

  def do_add_player({agent_game, game}, {player, status}, true) do
    PlayerRepository.save(player)
    result = GameOperations.add_player(game, {player, status})

    case result do
      {:ok, new_game} ->
        Agent.update(agent_game, fn _game -> new_game end)
        {_, game} = new_game.uuid |> get_game
        {game, player.nick_name, nil}

      {:error, message} ->
        {game, nil, message}
    end
  end

  def do_add_player({_agent_game, game}, _player, false), do: {game, nil, "El Nick ya existen"}

  def join(uuid, player) do
    join_results =
      case uuid |> get_game do
        nil ->
          {nil, nil, "El juego ya no está disponible"}

        {_agent, game} ->
          (game.actions |> Enum.member?(:add_player) && {game, nil, nil}) ||
            {game, nil, "Ya no puedes entrar al juego"}
      end

    case join_results do
      {game, nil, nil} ->
        add_player(game.uuid, {player, :pending})

      {game, _, error} ->
        {game, nil, error}
    end
  end

  def confirm_join(uuid, player) do
    {agent_game, game} = uuid |> get_game
    result = GameOperations.confirm_join(game, player)

    case result do
      {:ok, new_game} ->
        Agent.update(agent_game, fn _game -> new_game end)
        {_, game} = new_game.uuid |> get_game
        {game, player, nil}

      {:error, message} ->
        {game, nil, message}
    end
  end

  def cancel_join(uuid, player) do
    {agent_game, game} = uuid |> get_game
    result = GameOperations.cancel_join(game, player)

    case result do
      {:ok, new_game} ->
        Agent.update(agent_game, fn _game -> new_game end)
        {_, game} = new_game.uuid |> get_game
        {game, player, nil}

      {:error, message} ->
        {game, nil, message}
    end
  end

  def refresh(uuid, player) do
    result = uuid |> get_game

    case result do
      nil -> {nil, nil, nil}
      {_agent, game} -> {game, player, nil}
    end
  end

  def start(uuid, player) do
    {agent_game, game} = uuid |> get_game
    result = GameOperations.start(game)

    case result do
      {:ok, new_game} ->
        Agent.update(agent_game, fn _game -> new_game end)
        {_, game} = new_game.uuid |> get_game
        {game, player, nil}

      {:error, message} ->
        {game, player, message}
    end
  end

  def make_a_play(uuid, player, index_selected_domino, selected_side) do
    {agent_game, game} = uuid |> get_game

    selected_domino =
      index_selected_domino
      |> get_valid_selected_domino(game.valid_dominoes_to_play)

    selected_side =
      selected_side
      |> String.to_atom()

    result = GameOperations.make_a_play(game, selected_domino, selected_side)

    case result do
      {:ok, new_game} ->
        Agent.update(agent_game, fn _game -> new_game end)
        if new_game.status == :finished, do: GameRepository.save(new_game)
        {_, game} = new_game.uuid |> get_game
        {game, player, nil}

      {:error, message} ->
        {game, player, message}
    end
  end

  def change_turn(uuid, player) do
    {agent_game, game} = uuid |> get_game

    result = GameOperations.change_to_next_turn(game)

    case result do
      {:ok, new_game} ->
        Agent.update(agent_game, fn _game -> new_game end)
        if new_game.status == :finished, do: GameRepository.save(new_game)
        {_, game} = new_game.uuid |> get_game
        {game, player, nil}

      {:error, message} ->
        {game, player, message}
    end
  end

  def take_from_well(uuid, player) do
    {agent_game, game} = uuid |> get_game

    result = GameOperations.take_domino_from_well(game)

    case result do
      {:ok, new_game} ->
        Agent.update(agent_game, fn _game -> new_game end)
        {_, game} = new_game.uuid |> get_game
        {game, player, nil}

      {:error, message} ->
        {game, player, message}
    end
  end

  def rate_game(uuid, player, stars) do
    {agent_game, game} = uuid |> get_game

    result = GameOperations.rating(game, player, String.to_integer(stars))

    case result do
      {:ok, new_game} ->
        Agent.update(agent_game, fn _game -> new_game end)
        {_, game} = new_game.uuid |> get_game
        {game, player, nil}

      {:error, message} ->
        {game, player, message}
    end
  end

  def close(uuid, player) do
    case uuid |> get_game do
      {agent_game, game} ->
        result = GameOperations.close(game, player)

        case result do
          {:ok, new_game} ->
            Agent.update(agent_game, fn _game -> new_game end)
            GameRepository.save(new_game)
            {_, game} = new_game.uuid |> get_game
            if new_game.status == :close, do: teminate_agent_game(agent_game)
            {game, player, nil}

          {:error, message} ->
            {game, player, message}
        end

      nil ->
        {:error, "El juego no fue encontrado"}
    end
  end

  def get_game(uuid) do
    game =
      get_current_games()
      |> Enum.filter(fn {_agent_game, game} -> game.uuid == uuid end)

    case game do
      nil -> game
      _ -> List.first(game)
    end
  end

  def games_record_for_player(nick) do
    case player = PlayerRepository.find_by_nick(nick) do
      nil ->
        nil

      _ ->
        if player.email do
          player_games = GameRepository.games_for_a_player(player.uuid)
          total_games = Enum.count(player_games)
          won_games = get_won_games_for_player(player_games, player)
          percentage = if total_games > 0, do: won_games / total_games * 100, else: 0.0

          [
            total_games: total_games,
            won_games: won_games,
            lost_games: total_games - won_games,
            percentage: :erlang.float_to_binary(percentage, decimals: 2),
            message: get_message_for_record_game_by_percentage(percentage)
          ]
        else
          nil
        end
    end
  end

  defp get_won_games_for_player(player_games, player) do
    player_games
    |> Enum.filter(fn game -> List.first(game.positions).uuid == player.uuid end)
    |> Enum.count()
  end

  defp get_message_for_record_game_by_percentage(percentage) do
    messages_for_record_path = Application.get_env(:amazing_domino, :messages_for_record_path)

    messages_list =
      case File.read(messages_for_record_path) do
        {:ok, content} ->
          content
          |> String.split("\n", trim: true)

        _ ->
          [""]
      end

    case messages_list do
      [""] ->
        "Tu record"

      _ ->
        [_h | msgs] =
          cond do
            percentage >= 0.0 && percentage < 40 ->
              messages_list |> Enum.at(4) |> String.split("|", trim: true)

            percentage >= 40 && percentage < 60 ->
              messages_list |> Enum.at(3) |> String.split("|", trim: true)

            percentage >= 60 && percentage < 80 ->
              messages_list |> Enum.at(2) |> String.split("|", trim: true)

            percentage >= 80 && percentage < 100 ->
              messages_list |> Enum.at(1) |> String.split("|", trim: true)

            percentage == 100 ->
              messages_list |> List.first() |> String.split("|", trim: true)

            true ->
              ["", "Tu record"]
          end

        Enum.random(msgs)
    end
  end

  defp get_current_games do
    DynamicSupervisor.which_children(AmazingDomino.DynamicSupervisor)
    |> Enum.map(fn {_, agent, _, _} -> {agent, Agent.get(agent, & &1)} end)
  end

  defp get_valid_selected_domino(index_selected, valid_dominoes) do
    index = String.to_integer(index_selected)
    Enum.at(valid_dominoes, index)
  end

  defp teminate_agent_game(agent) do
    DynamicSupervisor.terminate_child(AmazingDomino.DynamicSupervisor, agent)
  end
end
