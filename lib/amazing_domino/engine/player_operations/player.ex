defmodule AmazingDomino.Engine.PlayerOperations.Player do
  defstruct [
    :uuid,
    :nick_name,
    :avatar,
    :email,
    dominoes: [],
    played_dominoes: []
  ]
end
