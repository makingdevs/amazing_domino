defmodule AmazingDomino.Engine.GameOperations do
  @moduledoc """
  This module handle the logic of AmazingDomino Game
  """

  alias AmazingDomino.Engine.GameOperations.Game
  alias AmazingDomino.Engine.PlayerOperations.Player
  alias AmazingDomino.Engine.PlayerOperations

  @doc """
  Creates a new game.
  Players is a list of Player struct. It could be empty.
  """
  # Create game
  # Actions: :add_player, :share, :close
  def create() do
    {:ok,
     %Game{
       uuid: UUID.uuid5(:oid, NaiveDateTime.to_string(NaiveDateTime.utc_now())),
       status: :created,
       players: [],
       dominoes: generate_dominoes(),
       actions: [:add_player, :share, :close]
     }}
  end

  def generate_dominoes do
    for i <- 0..6, j <- 0..6, i <= j, do: {i, j}
  end

  # Add player into game
  # Actions: :add_player, :start
  def add_player(%Game{status: :created} = the_game, {the_player, status_player}) do
    case Enum.count(the_game.players) do
      4 ->
        {:error, "Esta partida tiene 4 jugadores. Únete a otra partida o inicia un juego nuevo"}

      _ ->
        the_game
        |> player_exists?(the_player)
        |> add_new_player(the_game, the_player, status_player)
    end
  end

  def add_player(%Game{status: _}, _),
    do: {:error, "Can't add the player because status is not valid"}

  def player_exists?(the_game, new_player) do
    not (the_game.players
         |> Enum.find(fn {player, _status} -> player.nick_name == new_player.nick_name end)
         |> is_nil)
  end

  def add_new_player(true, _, new_player, _),
    do: {:error, "El jugador --#{new_player.nick_name}-- ya existe"}

  def add_new_player(false, the_game, new_player, status_player) do
    {:ok, %Game{the_game | players: the_game.players ++ [{new_player, status_player}]}}
    |> define_actions_to_do
  end

  # Confirm join
  def confirm_join(%Game{status: :created} = the_game, player_joining) do
    index_player =
      the_game.players
      |> Enum.find_index(fn {player, _status} -> player.nick_name == player_joining end)

    {the_player, _status} =
      the_game.players
      |> Enum.find(fn {player, _status} -> player.nick_name == player_joining end)

    confirmed_player = {the_player, :confirmed}

    updated_players =
      the_game.players
      |> List.replace_at(index_player, confirmed_player)

    {:ok, %Game{the_game | players: updated_players}}
  end

  # Cancel join
  def cancel_join(%Game{status: :created} = the_game, player_joining) do
    new_players =
      the_game.players
      |> Enum.filter(fn {player, _status} -> player.nick_name != player_joining end)

    {:ok, %Game{the_game | players: new_players}}
    |> define_actions_to_do
  end

  def cancel_join(%Game{status: _}, _), do: {:error, "El juego ya fue iniciado"}

  # Starting game
  # actions: :play
  def start(the_game), do: check_total_players(the_game, Enum.count(the_game.players))

  def check_total_players(_, total_players) when total_players < 2,
    do: {:error, "Not enough players yet"}

  def check_total_players(the_game, total_players) when total_players >= 2, do: starting(the_game)

  def starting(%Game{status: :ordered} = the_game),
    do: {:ok, %Game{the_game | status: :started, actions: [:play]}}

  def starting(%Game{status: :created} = the_game) do
    {:ok, the_game} = delete_not_confirmed_players(the_game)
    {:ok, delivered_game} = hand_out_dominoes(the_game)
    starting(delivered_game)
  end

  def starting(%Game{status: :delivered} = the_game) do
    {:ok, ordered_game} =
      the_game
      |> define_first_turn
      |> get_validate_dominoes_to_play
      |> set_initial_domino

    starting(ordered_game)
  end

  def starting(%Game{status: _}),
    do: {:error, "Can't start the game because current status is not valid"}

  def delete_not_confirmed_players(the_game) do
    players_confirmed =
      the_game.players
      |> Enum.filter(fn {_player, status} -> status == :confirmed end)

    the_game = %Game{the_game | players: players_confirmed}
    {:ok, the_game}
  end

  # Hand out dominoes into game
  def hand_out_dominoes(%Game{status: :created} = the_game) do
    {dominoes, players} = hand_out_dominoes_to_players({the_game.dominoes, the_game.players, []})
    {:ok, %Game{the_game | dominoes: dominoes, players: players, status: :delivered}}
  end

  def hand_out_dominoes(%Game{status: _}),
    do: {:error, "Can't hand out dominoes because current status isn't valid"}

  def hand_out_dominoes_to_players({dominoes, [], players}), do: {dominoes, players}

  def hand_out_dominoes_to_players({dominoes, [{player, status} | rest], already_players}) do
    player_dominoes = Enum.take_random(dominoes, 7)

    the_player = {%Player{player | dominoes: player_dominoes}, status}
    available_dominoes = dominoes -- player_dominoes
    hand_out_dominoes_to_players({available_dominoes, rest, [the_player | already_players]})
  end

  # Define first turn
  def define_first_turn(the_game) do
    players_with_mules =
      the_game.players
      |> Enum.filter(fn {player, _status} ->
        not Enum.empty?(PlayerOperations.get_mules_dominoes(player))
      end)

    {_, first_turn} = calculate_first_turn(players_with_mules, the_game.players, :mules)
    {:ok, %Game{the_game | current_turn: first_turn, status: :ordered}}
  end

  def calculate_first_turn([], players, :mules), do: calculate_first_turn([], players, :no_mules)

  def calculate_first_turn(_players_with_mules, players, :mules) do
    players
    |> Enum.map(fn {player, _status} -> player end)
    |> Enum.with_index()
    |> Enum.sort(fn {pl1, _}, {pl2, _} ->
      PlayerOperations.get_max_points_mules(pl1) > PlayerOperations.get_max_points_mules(pl2)
    end)
    |> List.first()
  end

  def calculate_first_turn(_players_with_mules, players, :no_mules) do
    players
    |> Enum.map(fn {player, _status} -> player end)
    |> Enum.with_index()
    |> Enum.sort(fn {pl1, _}, {pl2, _} ->
      PlayerOperations.get_max_points(pl1) > PlayerOperations.get_max_points(pl2)
    end)
    |> List.first()
  end

  def get_validate_dominoes_to_play({:ok, the_game}) do
    valid_dominoes =
      the_game
      |> get_player_in_turn
      |> PlayerOperations.get_validate_dominoes_to_play(the_game.board)

    {:ok, %Game{the_game | valid_dominoes_to_play: valid_dominoes}}
  end

  def get_validate_dominoes_to_play({:error, _} = error), do: error

  def set_initial_domino({:ok, the_game}) do
    {:ok, %Game{the_game | initial_domino: List.first(the_game.valid_dominoes_to_play)}}
  end

  def set_initial_domino({:error, _} = error), do: error

  # Close game
  # Actions: :none
  def close(the_game, the_player) do
    index_player =
      the_game.players
      |> Enum.find_index(fn {player, _status} -> player.nick_name == the_player end)

    {the_player, _status} =
      the_game.players |> Enum.find(fn {player, _status} -> player.nick_name == the_player end)

    exit_player = {the_player, :exit}

    updated_players =
      the_game.players
      |> List.replace_at(index_player, exit_player)

    players_not_exited =
      updated_players
      |> Enum.count(fn {_player, status} -> status != :exit end)

    new_status = if players_not_exited == 0, do: :close, else: the_game.status

    {:ok, %Game{the_game | players: updated_players, status: new_status}}
  end

  # Rating the game
  def rating(the_game, player, stars) do
    new_rating = add_rating_game(the_game.rating, player, stars)
    {:ok, %Game{the_game | rating: new_rating}}
  end

  def add_rating_game([] = rating, player, stars), do: rating ++ [{player, stars}]

  def add_rating_game(rating, player, stars) do
    case player in Enum.map(rating, fn {player, _} -> player end) do
      false ->
        rating ++ [{player, stars}]

      true ->
        rate_player_index =
          rating
          |> Enum.map(fn {pl, _} -> pl end)
          |> Enum.find_index(fn pl -> pl == player end)

        List.replace_at(rating, rate_player_index, {player, stars})
    end
  end

  # Share game
  def share(the_game) do
    the_game.uuid
  end

  # Change to next turn
  # actions: :play, :take_from_well, :change_turn
  def change_to_next_turn(%Game{status: :started} = the_game) do
    {:ok, the_game} =
      {:ok, %Game{the_game | current_turn: calculate_next_turn(the_game)}}
      |> get_validate_dominoes_to_play
      |> define_actions_to_do

    finish_game(the_game)
  end

  def change_to_next_turn(%Game{status: :finished}) do
    {:error, "Game is over"}
  end

  def change_to_next_turn(%Game{status: _}) do
    {:error, "Game hasn't valid status"}
  end

  def calculate_next_turn(the_game) do
    case the_game.current_turn == Enum.count(the_game.players) - 1 do
      true -> 0
      _ -> the_game.current_turn + 1
    end
  end

  # Define actions game
  def define_actions_to_do({:ok, %Game{status: :created} = the_game}) do
    total_players = Enum.count(the_game.players)

    actions =
      cond do
        total_players >= 2 && total_players < 4 -> [:add_player, :start]
        total_players < 2 -> [:add_player]
        total_players == 4 -> [:start]
      end

    {:ok, %Game{the_game | actions: actions}}
  end

  def define_actions_to_do({:ok, %Game{status: :started} = the_game}) do
    actions =
      cond do
        Enum.empty?(the_game.valid_dominoes_to_play) && Enum.empty?(the_game.dominoes) ->
          [:change_turn]

        Enum.empty?(the_game.valid_dominoes_to_play) && not Enum.empty?(the_game.dominoes) ->
          [:take_from_well]

        not Enum.empty?(the_game.valid_dominoes_to_play) ->
          [:play]
      end

    {:ok, %Game{the_game | actions: actions}}
  end

  def define_actions_to_do({:error, _} = error), do: error

  # Get player in turn
  def get_player_in_turn(the_game) do
    the_game.players
    |> Enum.map(fn {player, _status} -> player end)
    |> Enum.at(the_game.current_turn)
  end

  # Make a play
  # actions: :change_turn
  def make_a_play(the_game, selected_domino, selected_side) do
    case put_domino_in_board(the_game.board, selected_domino, selected_side) do
      [{:board, _}, {:error, message}] ->
        {:error, message}

      [{:board, new_board}, {:matched_domino, matched_domino}, {:ok, _}] ->
        {:ok, new_game} =
          update_game_with_new_board_and_players(
            the_game,
            new_board,
            selected_domino,
            matched_domino
          )

        finish_game(new_game)
    end
  end

  def update_game_with_new_board_and_players(the_game, new_board, selected_domino, matched_domino) do
    updated_players =
      the_game
      |> move_domino_of_player_in_turn(selected_domino, matched_domino)

    {:ok,
     %Game{
       the_game
       | board: new_board,
         players: updated_players,
         valid_dominoes_to_play: [],
         actions: [:confirm_play]
     }}
  end

  def put_domino_in_board([] = board, domino, _),
    do: [board: board ++ [domino], matched_domino: domino, ok: ""]

  def put_domino_in_board(board, domino, :right) do
    {_, right_board_domino} = List.last(board)
    matched_domino = match_domino_to_right(right_board_domino, domino)

    case matched_domino do
      {:error, message} -> [board: board, error: message]
      _ -> [board: board ++ [matched_domino], matched_domino: matched_domino, ok: ""]
    end
  end

  def put_domino_in_board(board, domino, :left) do
    {left_board_domino, _} = List.first(board)
    matched_domino = match_domino_to_left(left_board_domino, domino)

    case matched_domino do
      {:error, message} -> [board: board, error: message]
      _ -> [board: [matched_domino | board], matched_domino: matched_domino, ok: ""]
    end
  end

  def match_domino_to_right(right_board_domino, {right_board_domino, _} = domino), do: domino

  def match_domino_to_right(right_board_domino, {left_domino, right_board_domino}),
    do: {right_board_domino, left_domino}

  def match_domino_to_right(_right_board_domino, {_left_domino, _right_domino}),
    do: {:error, "Ficha no válida en esta posición"}

  def match_domino_to_left(left_board_domino, {_, left_board_domino} = domino), do: domino

  def match_domino_to_left(left_board_domino, {left_board_domino, right_domino}),
    do: {right_domino, left_board_domino}

  def match_domino_to_left(_left_board_domino, {_left_domino, _right_domino}),
    do: {:error, "Ficha no válida en esta posición"}

  def move_domino_of_player_in_turn(the_game, selected_domino, matched_domino) do
    player_in_turn = get_player_in_turn(the_game)

    updated_player =
      PlayerOperations.play_a_domino(player_in_turn, selected_domino, matched_domino)

    the_game.players
    |> List.replace_at(the_game.current_turn, {updated_player, :confirmed})
  end

  # Take domino from well
  # actions: :play, :change_turn, :take_from_well
  def take_domino_from_well(the_game) do
    the_game
    |> check_dominoes_well(the_game.dominoes)
    |> get_validate_dominoes_to_play
    |> define_actions_to_do
  end

  def check_dominoes_well(_, []), do: {:error, "The well is empty"}

  def check_dominoes_well(the_game, well) do
    well_domino = Enum.take_random(well, 1)
    new_dominoes = well -- well_domino

    updated_players =
      the_game
      |> player_take_domino_from_well(well_domino)

    {:ok, %Game{the_game | dominoes: new_dominoes, players: updated_players}}
  end

  def player_take_domino_from_well(the_game, well_domino) do
    player_in_turn = get_player_in_turn(the_game)
    updated_player = PlayerOperations.add_a_domino(player_in_turn, well_domino)

    the_game.players
    |> List.replace_at(the_game.current_turn, {updated_player, :confirmed})
  end

  # Finish game
  # actions: :change_turn, :none
  def finish_game(the_game) do
    do_finish_game(
      the_game,
      is_game_over_by_dominoes_exhausted?(the_game),
      is_game_over_by_closed_game?(the_game)
    )
  end

  def do_finish_game(the_game, true, _), do: finished_game(the_game)
  def do_finish_game(the_game, _, true), do: finished_game(the_game)
  def do_finish_game(the_game, false, false), do: {:ok, the_game}

  def is_game_over_by_dominoes_exhausted?(the_game) do
    the_game.players
    |> Enum.count(fn {player, _status} -> Enum.empty?(player.dominoes) end) >
      0
  end

  def is_game_over_by_closed_game?(the_game) do
    the_game.players
    |> Enum.count(fn {player, _status} ->
      not Enum.empty?(PlayerOperations.get_validate_dominoes_to_play(player, the_game.board))
    end) ==
      0 && the_game.dominoes |> Enum.empty?()
  end

  def finished_game(the_game) do
    positions = the_game |> get_sort_players_positions
    winner = positions |> List.first()

    {:ok,
     %Game{the_game | status: :finished, actions: [:none], positions: positions, winner: winner}}
  end

  # Game results
  def get_sort_players_positions(the_game) do
    the_game.players
    |> Enum.map(fn {player, _status} -> player end)
    |> Enum.sort(fn pl1, pl2 ->
      PlayerOperations.get_current_points(pl1) + Enum.count(pl1.dominoes) <
        PlayerOperations.get_current_points(pl2) + Enum.count(pl2.dominoes)
    end)
  end

  def get_winner_game(the_game) do
    the_game
    |> get_sort_players_positions
    |> List.first()
  end
end
