defmodule AmazingDomino.Engine.PlayerOperations do
  alias AmazingDomino.Engine.PlayerOperations.Player

  def create(user) do
    %Player{
      uuid: UUID.uuid5(:oid, NaiveDateTime.to_string(NaiveDateTime.utc_now())),
      nick_name: user.nick,
      avatar: user.avatar,
      email: user.email
    }
  end

  def show_dominoes(player), do: player.dominoes

  def get_validate_dominoes_to_play(player, []) do
    mules_dominoes = get_mules_dominoes(player)

    max_points =
      case mules_dominoes do
        [] -> get_max_points(player)
        _ -> get_max_points_mules(player)
      end

    do_get_validate_dominoes_to_play({player.dominoes, mules_dominoes, max_points})
  end

  def get_validate_dominoes_to_play(player, board) do
    do_get_validate_dominoes_to_play({player.dominoes, board})
  end

  def do_get_validate_dominoes_to_play({dominoes, [], max_points}),
    do: Enum.filter(dominoes, fn {x, y} -> x + y == max_points end)

  def do_get_validate_dominoes_to_play({_dominoes, mules_dominoes, max_points}),
    do: Enum.filter(mules_dominoes, fn {x, y} -> x + y == max_points end)

  def do_get_validate_dominoes_to_play({dominoes, board}) do
    {left_board, _} = List.first(board)
    {_, right_board} = List.last(board)

    dominoes
    |> Enum.filter(fn {x, y} ->
      x == left_board or x == right_board or y == left_board or y == right_board
    end)
  end

  def play_a_domino(player, selected_domino, matched_domino) do
    new_dominoes = player.dominoes -- [selected_domino]
    played_dominoes = [matched_domino | player.played_dominoes]
    %Player{player | dominoes: new_dominoes, played_dominoes: played_dominoes}
  end

  def add_a_domino(player, domino) do
    %Player{player | dominoes: player.dominoes ++ domino}
  end

  def get_current_points(player) do
    player.dominoes
    |> Enum.map(fn {x, y} -> x + y end)
    |> Enum.sum()
  end

  def get_max_points(player) do
    player.dominoes |> get_points_dominoes |> Enum.max()
  end

  def get_points_dominoes([]), do: [0]

  def get_points_dominoes(dominoes) do
    Enum.map(dominoes, fn {x, y} -> x + y end)
  end

  def get_mules_dominoes(player) do
    player.dominoes |> Enum.filter(fn {x, y} -> x == y end)
  end

  def get_max_points_mules(player) do
    player |> get_mules_dominoes |> get_points_dominoes |> Enum.max()
  end
end
