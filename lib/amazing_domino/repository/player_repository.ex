defmodule AmazingDomino.Repository.PlayerRepository do
  alias AmazingDomino.Repository.PlayerRepository.Player

  def save(player) do
    Memento.transaction!(fn ->
      Memento.Query.write(%Player{
        uuid: player.uuid,
        nick_name: player.nick_name,
        avatar: player.avatar,
        email: player.email
      })
    end)
  end

  def list() do
    Memento.transaction!(fn ->
      Memento.Query.all(Player)
    end)
  end

  def find_by_nick(nick) do
    Memento.transaction!(fn ->
      Memento.Query.select(Player, {:==, :nick_name, nick})
    end)
    |> List.first()
  end

  def valid_player?(nick) do
    is_nil(find_by_nick(nick))
  end

  def player_existing(nick) do
    existing_player = find_by_nick(nick)
    do_player_existing(existing_player)
  end

  def do_player_existing(nil), do: nil

  def do_player_existing(existing_player) do
    existing_player.uuid
  end
end
