defmodule AmazingDomino.Repository.GameRepository do
  alias AmazingDomino.Repository.GameRepository.Game

  def save(game) do
    Memento.transaction!(fn ->
      Memento.Query.write(%Game{
        uuid: game.uuid,
        positions: game.positions,
        dominoes: game.dominoes,
        board: game.board,
        rating: game.rating
      })
    end)
  end

  def list() do
    Memento.transaction!(fn ->
      Memento.Query.all(Game)
    end)
  end

  def get(uuid) do
    Memento.transaction!(fn ->
      Memento.Query.read(Game, uuid)
    end)
  end

  def games_for_a_player(player_uuid) do
    players_uuids = fn players -> Enum.map(players, fn p -> p.uuid end) end
    player_is_in_game? = fn g, uuid -> Enum.member?(players_uuids.(g.positions), uuid) end
    for game <- list(), player_is_in_game?.(game, player_uuid), do: game
  end
end
