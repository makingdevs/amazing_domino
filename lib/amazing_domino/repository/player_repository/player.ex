defmodule AmazingDomino.Repository.PlayerRepository.Player do
  use Memento.Table, attributes: [:uuid, :nick_name, :avatar, :email]
end
