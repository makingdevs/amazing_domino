defmodule AmazingDomino.Repository.GameRepository.Game do
  use Memento.Table, attributes: [:uuid, :positions, :dominoes, :board, :rating]
end
