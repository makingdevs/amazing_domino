defmodule AmazingDomino.Repo do
  use GenServer

  def start_link(opts) do
    GenServer.start_link(__MODULE__, [], opts)
  end

  def init(_) do
    setup!()
    {:ok, []}
  end

  def setup!() do
    nodes = [node()]

    Memento.stop()
    Memento.Schema.create(nodes)
    Memento.start()

    unless AmazingDomino.Repository.GameRepository.Game in Memento.system(:tables),
      do: Memento.Table.create!(AmazingDomino.Repository.GameRepository.Game, disc_copies: nodes)

    unless AmazingDomino.Repository.PlayerRepository.Player in Memento.system(:tables),
      do:
        Memento.Table.create!(AmazingDomino.Repository.PlayerRepository.Player, disc_copies: nodes)
  end
end
