defmodule AmazingDomino.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      AmazingDominoWeb.Endpoint,
      {DynamicSupervisor, strategy: :one_for_one, name: AmazingDomino.DynamicSupervisor},
      AmazingDomino.Repo
    ]

    opts = [strategy: :one_for_one, name: AmazingDomino.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def config_change(changed, _new, removed) do
    AmazingDominoWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
