# Amazing Domino

## Iniciar un juego
### Criterios de aceptación
- Iniciar todas las fichas
- Iniciar el estado del juego
- Ver el estado del juego
- Se inicia con por lo menos 2 jugadores

## Compartir el juego
### Criterios de aceptación
- Tener un identifcador único del juego
- Tener un elemento que ayude a compartir

## Agregar un jugador
### Criterios de aceptación
- Mínimo 2 jugadores
- Máximo 4 jugadores
- Define el orden de turnos

## Repartir fichas
### Criterios de aceptación
- Dar 6 fichas a cada jugador

## Ver mis fichas
### Criterios de aceptación
- Dar 6 fichas a cada jugador

## Turnar un jugador
### Criterios de aceptación
- Ceder el control del juego a un jugador

## Hacer una jugada
### Criterios de aceptación
- Coloca una ficha "válida" en el tablero
- Confirmar la jugada

## Comer fichas
### Criterios de aceptación
- Si un jugador no tiene una ficha "válida" para jugar entonces toma del pozo
- El jugador toma tantas fichas hasta encontrar una "válida"
- Si ya no hay fichas en el pozo entonces pasa su turno

## Pasar turno
### Criterios de aceptación
- Si el jugador no tiene alguna ficha "válida" entonces no puede jugar
- El siguiente jugador tendrá oportunidad de jugar

## Terminar un juego por que alguien se acabo las fichas
### Criterios de aceptación
- Cuando un jugador se termina sus fichas entonces gana

## Terminar un juego por que se cerro
### Criterios de aceptación
- Cuando los jugadores ya no tienen fichas que correspondan a la siguiente posibilidad en el juego, entonces se acaba el juego y se cuentan los puntos

## Registro de actividad de los jugadores
## Perfil del jugador

