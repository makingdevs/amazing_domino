# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

# Configures the endpoint
config :amazing_domino, AmazingDominoWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: System.get_env("SECRET_KEY_BASE"),
  render_errors: [view: AmazingDominoWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: AmazingDomino.PubSub, adapter: Phoenix.PubSub.PG2],
  live_view: [signing_salt: System.get_env("SIGNING_SALT")]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Mnesia config
config :mnesia,
  dir: '.amazing_domino/#{Mix.env()}/#{node()}'

config :recaptcha,
  public_key: System.get_env("RECAPTCHA_PUBLIC_KEY"),
  secret: System.get_env("RECAPTCHA_SECRET")

config :recaptcha, :json_library, Poison

config :amazing_domino, messages_for_record_path: System.get_env("MESSAGES_RECORD_FILE")

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
