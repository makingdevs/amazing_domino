defmodule AmazingDomino.MixProject do
  use Mix.Project

  def project do
    [
      app: :amazing_domino,
      version: "0.1.0",
      elixir: "~> 1.5",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix, :gettext] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  def application do
    [
      mod: {AmazingDomino.Application, []},
      extra_applications: [:logger, :runtime_tools, :recaptcha]
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  defp deps do
    [
      {:elixir_uuid, "~> 1.2"},
      {:phoenix, "~> 1.4.16"},
      {:phoenix_pubsub, "~> 1.1"},
      {:phoenix_html, "~> 2.11"},
      {:phoenix_live_reload, "~> 1.2", only: :dev},
      {:gettext, "~> 0.11"},
      {:jason, "~> 1.0"},
      {:plug_cowboy, "~> 2.0"},
      {:phoenix_live_view, "~> 0.11.0"},
      {:memento, "~> 0.3.1"},
      {:recaptcha, "~> 3.0"},
      {:poison, "~> 3.1"}
    ]
  end
end
